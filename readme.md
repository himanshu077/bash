

## BASH API setup instructions for local and server dev

Extract code from gitlab:
```bash
git clone https://gitlab.com/zkinion/bashapi.git
```
Copy .env.example to .env, and setup any variables for your database.
```bash
cp .env.example .env
nano .env
```
Install composer and npm:
```bash
composer install
npm update

```


Copy your database over or run migration to get all the schema and seed generation:
```bash
php artisan migrate
```