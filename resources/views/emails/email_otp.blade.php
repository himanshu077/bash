<!DOCTYPE html>
<html>
<head>
<style>
.email_wrapper {background-color: #f2c0f1; text-align: center; padding: 22px; width: 600px; margin: auto;}
.email_inner_content h1 {font-size: 34px; color: #41003d; display: inline-block; border-bottom: 2px solid #b68bb6; padding-bottom: 8px; margin-bottom: 22px; margin-top: 20px;}
.email_inner_content p {font-size: 22px; color: #56535a; margin-bottom: 20px;}
.email_top_logo img {max-width: 100%;}
.email_top_logo {max-width: 50px; margin: auto auto 15px auto;}
.Email_Code {display: inline-block; background: #7e007d; color: #fff; padding: 10px 34px; text-decoration: none; border-radius: 24px;}
.email_inner_content p.verification_code {margin-bottom: 12px;}
.email_inner_content p.click_here {margin: 30px 0px 6px 0px; padding: 45px 10px 0px 10px; border-top: 2px solid #b68bb6; display: inline-block;}
.email_footer_logo img {max-width: 100%;}	
.email_inner {border: 2px solid #f5c7b8; padding:30px;}
.click_here a {color: #7e007d; text-decoration: none;}
.email_footer_logo {margin: 50px 0px;}
</style>
</head> 
	<body>
	<div class="email_wrapper">
	  <div class="email_inner">
		<div class="email_top_logo">
		  <a href=""><img src="<?=URL::to('/')?>/public/bash_icon.png" /></a>
		</div>
		<div class="email_inner_content">
		  <h1>Verify Your Email</h1>
		  <p>Please verify your email to login.</p>
		  <p class="verification_code">Enter the verification code in the app.</p>
		  <div class="email_code_main">
		    <a href="" class="Email_Code">{{ $otp }}</a>
		  </div>
		  <!--<p class="click_here">If this wasn't you, <a href="" class="email_link">Please click here</a></p>-->
		</div>
		<div class="email_footer_logo">
		  <img src="<?=URL::to('/')?>/public/bash_icon1.png" />
		  <img src="<?=URL::to('/')?>/public/bash_icon_192.png" />
		</div>
	  </div>
	</div>
	</body>
</html>
