Bash Mobile Email Verification
Verify your email address by clicking or copying/pasting this link:
{{ route('email.verify') }}?token={{ $token }}