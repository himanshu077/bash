
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Bash Mobile</h1>
            <p>Verify your email address: </p>
            <a href="{{ route('email.verify') }}?token={{ $token }}" >Click this link</a>
        </div>
    </div>
</div>
