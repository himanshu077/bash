<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Help</title>
    <style type="text/css">
      @font-face {
    font-family: 'Avenir LT 65';
    src: url('public/images/help/fonts/AvenirLT-Medium.eot');
    src: url('public/images/help/fonts/AvenirLT-Medium.eot?#iefix') format('embedded-opentype'),
        url('public/images/help/fonts/AvenirLT-Medium.woff2') format('woff2'),
        url('public/images/help/fonts/AvenirLT-Medium.woff') format('woff'),
        url('public/images/help/fonts/AvenirLT-Medium.ttf') format('truetype'),
        url('public/images/help/fonts/AvenirLT-Medium.svg#AvenirLT-Medium') format('svg');
    font-weight: 500;
    font-style: normal;
}


      body { margin: 0; padding: 0; font-family: 'Avenir LT 65';  }
      .help_main_box { width: 100%; max-width: 1100px; margin: 0 auto;  display: flex;}
      .help_main_box_main { width: 100%; padding-left: 15px; padding-right: 15px;}
      .help_box_content { width: 100%; margin-bottom: 30px;}
      .help_box_content h1{ font-size: 20px; font-weight: bold; margin: 0 0 15px 0; padding: 0; }
      .help_box_content p{ font-size: 20px; font-weight: normal; margin: 0 0 10px 0; padding: 0; }
      .help_box_content p span { display: inline-block; vertical-align: middle; margin: 0 5px; }
	  .help_box_content p span img{ height:36px; }
        </style>
  </head>
  <body>
    <div class="help_main_box">
      <div class="help_main_box_main">
        <div class="help_box_content">
          <h1>Finding events:</h1>
          <p>Events can be easily viewed on the map as<span><img src="public/images/help/image_1.png" alt="image" /></span><span><img src="public/images/help/image_2.png" alt="image" /></span><span><img src="public/images/help/image_3.png" alt="image" /></span></p>
          <p>Click on Bash Hub icon on the bottom of the home page to get a list view of events in the area.</p>
          <p>Increase your event radius from BASH preferences to see more events.</p>
          <p>To look at future events, click<span><img src="public/images/help/image_4.png" alt="image" /></span>on the bottom left corner of the home page.</p>
        </div>
        <div class="help_box_content">
          <h1>Attending an event:</h1>
          <p>Click on the event icon on the map to get more details.</p>
          <p>Click Crash the BASH to RSVP to the event or buy a tickets using Venmo, Apple Pay, Google Pay or Credit Cards.</p>
          <p>These tickets and event details will then be available in your My BASH page which can be accessed by clicking<span><img src="public/images/help/image_5.png" alt="image" /></span>on the top right button on the home screen.</p>
        </div>
        <div class="help_box_content">
          <h1>Hosting an event:</h1>
          <p>Click<span><img src="public/images/help/image_6.png" alt="image" /></span>on home page to host an event.</p>
          <p>Cover charges collected from the event are reflected in the Payments page, accessed through the main menu.</p>
        </div>
        <div class="help_box_content">
          <h1>Check-in guests:</h1>
          <p>Click on the check in option in the menu to see every guest that has RSVP’d or purchased tickets to your event.</p>
          <p>Click on Scan ticket to scan the QR code and check guests in. Once a guest is successfully checked in, a green check mark will appear next to their name.</p>
          <p>For a free event, users will check themselves in. The roster of people RSVP’d to the event and attending the event will be available in the Check-in page.</p>
        </div>
        <div class="help_box_content">
          <h1>Payments:</h1>
          <p>A transaction fee of 30 cents + 2.7% Is charged by Venmo, Google Pay, Apple Pay and Braintree for each transaction. </p>
          <p>Once the transaction is cleared, the amount is reflected in the BASH balance and can be transferred from BASH Balance to your paypal account.</p>
        </div>
      </div>
    </div>
  </body>
</html>