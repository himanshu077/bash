<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191)->nullable();
			$table->string('email', 191)->nullable()->unique();
			$table->dateTime('email_verified_at')->nullable();
			$table->string('phone_number', 64)->nullable();
			$table->string('password', 191)->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->string('session_id', 32)->nullable();
			$table->integer('linked_fb')->nullable()->default(0);
			$table->string('linked_snap')->nullable();
			$table->integer('userType')->nullable()->default(0)->comment('\'1\'=>\'app\'');
			$table->string('fname', 191)->nullable();
			$table->string('lname', 191)->nullable();
			$table->date('dob')->nullable();
			$table->string('username', 50)->nullable();
			$table->string('country_code', 30)->nullable();
			$table->string('otp', 30)->nullable();
			$table->string('device_token')->nullable();
			$table->string('device_type', 30)->nullable();
			$table->string('social_id', 150)->nullable();
			$table->string('gender', 30)->nullable();
			$table->string('address', 500)->nullable();
			$table->string('image')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
