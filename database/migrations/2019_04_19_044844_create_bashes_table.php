<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBashesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bashes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->index('user_id');
			$table->integer('bash_type')->nullable();
			$table->string('name', 191)->nullable();
			$table->string('name_of_host', 191)->nullable();
			$table->date('start_date')->nullable();
			$table->time('start_time')->nullable();
			$table->time('end_time')->nullable();
			$table->string('location', 500)->nullable();
			$table->string('lat', 100)->nullable();
			$table->string('lng', 100)->nullable();
			$table->decimal('charge', 10)->nullable();
			$table->string('age', 30)->nullable();
			$table->timestamps();
			$table->string('session_id', 191)->nullable();
			$table->integer('is_private')->nullable();
			$table->text('private_users', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bashes');
	}

}
