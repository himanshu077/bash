<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCrashesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('crashes', function(Blueprint $table)
		{
			$table->foreign('bash_id', 'crashes_ibfk_1')->references('id')->on('bashes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'crashes_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('crashes', function(Blueprint $table)
		{
			$table->dropForeign('crashes_ibfk_1');
			$table->dropForeign('crashes_ibfk_2');
		});
	}

}
