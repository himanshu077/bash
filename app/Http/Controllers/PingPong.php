<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use RedisClient\RedisClient;
use RedisClient\Client\Version\RedisClient2x6;
use RedisClient\ClientFactory;

use App\Bash;
use App\User;
use App\Crash;

class PingPong extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }


    public function ping(Request $request){

        $session_id = $_REQUEST['session_id'];
        $lat = (float)  $_REQUEST['lat'];
        $long = (float) $_REQUEST['long'];

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $return_value = $Redis->geoadd("users.geo", array($session_id => array($long, $lat)));
        //echo json_encode($return_value);
        api_response(200,'');
    }


    public function debug_get_all(Request $request){

        if(!isset($_REQUEST['key'])){
            $key = 'users.geo';
        }else{
            $key = $_REQUEST['key'];
        }

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $keys = $Redis->keys('users.geo');

        $type = $Redis->type('users.geo');

        $data = $Redis->zrange($key, 0, -1, true);
        echo json_encode($data);

        die(0);
    }


    public function radius(Request $request){

        if(!isset($_REQUEST['radius'])){
            $radius = 100;
        }else{
            $radius = (int) $_REQUEST['radius'];
        }

        $session_id = $_REQUEST['session_id'];

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $data = $Redis->georadiusbymember("users.geo", $session_id, $radius, "mi", true, true, true);

        echo json_encode($data);

        die(0);
    }


    public function raw_radius(Request $request){

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $lat = (float) $_REQUEST['lat'];
        $long = (float) $_REQUEST['long'];
        $radius = (int) $_REQUEST['radius'];
        $return = $_REQUEST['return'];

        $data = $Redis->georadius("users.geo", $long, $lat, $radius, "mi", true, true, false);

        if($return == null || $return == false){
            echo json_encode($data);

            die(0);
        }else{
            return $data;
        }
    }


    public function init_map(Request $request){

        $my_session_id = (string) $_REQUEST['session_id'];
        $lat = (float) $_REQUEST['lat'];
        $long = (float) $_REQUEST['long'];
        $radius = (int) $_REQUEST['radius'];

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $add_myself_to_map = $Redis->geoadd("users.geo", array($my_session_id => array($long, $lat)));

        $data_arr = array();
        $data = $Redis->georadius("users.geo", $long, $lat, $radius, "mi", true, true, false);
         
        foreach($data as $key => &$value){ //print_r($key); die;


            if (strpos($key, "user") != false) {
                $value['data'] = User::where('session_id', '=', $key)->first();
                $value['type'] = "user";
            }
            if (strpos($key, "bash") != false) {
                $value['data'] = Bash::where('session_id', '=', $key)->first();
                $value['type'] = "bash";
            }
            if (strpos($key, "crash") != false) {
                $value['data'] = Crash::where('session_id', '=', $key)->first();
                $value['type'] = "crash";
            }
            $data_arr[] = $value;
        }


        echo json_encode($data_arr);
        die(0);
    }


    public function get_location(Request $request){

        $str_var = $_REQUEST["session_id"];
        $session_ids_array = explode(",", $str_var);

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $geo_data = array();

        foreach($session_ids_array as $session_id){
            $geo_data[$session_id] = $Redis->geopos("users.geo", $session_id);
        }

        echo json_encode($geo_data);

        die(0);
    }


    public function get_user(Request $request){

        $Redis = new RedisClient([
            'server' => '127.0.0.1:6379',
            'timeout' => 4
        ]);

        $session_id = $_REQUEST['session_id'];

        $geo_data = $Redis->geopos("users.geo", array($session_id));

        $user = User::where('session_id', '=', $session_id)->first();
        $user = $user->toArray();
        $user['geo_data'] = $geo_data;
        echo json_encode($user);
        die(0);
    }


    public function get_bash(Request $request){

        $session_id = $_REQUEST['session_id'];
        $bash = Bash::where('session_id', '=', $session_id)->first();
        echo json_encode($bash);
        die(0);

    }


    public function get_crash(Request $request){

        $session_id =  $_REQUEST['session_id'];
        $bash = Crash::where('session_id', '=', $session_id)->first();
        echo json_encode($bash);
        die(0);

    }


}
