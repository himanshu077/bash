<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Redis;
use Keygen\Keygen;

use PHPMailer\PHPMailer\PHPMailer;

class EmailController extends Controller
{
    public function sendTestEmail(Request $requet, $email_to){

        $to = $email_to;
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->setFrom(env('AWS_FROM_EMAIL'), 'BASH');
        $mail->addAddress($email_to, '');
        $mail->Username = env('MAIL_USERNAME');
        $mail->Password = env('MAIL_PASSWORD');
        //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
        $mail->Host = env('MAIL_HOST');
        $mail->Subject = 'Amazon SES test (SMTP interface accessed using PHP)';

        $mail->Body = '<h1>Email Test</h1>
    <p>This email was sent through the 
    <a href="https://aws.amazon.com/ses">Amazon SES</a> SMTP
    interface using the <a href="https://github.com/PHPMailer/PHPMailer">
    PHPMailer</a> class.</p>';
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
       $mail->isHTML(true);

        $mail->AltBody = "Email Test\r\nThis email was sent through the 
    Amazon SES SMTP interface using the PHPMailer class.";

        if(!$mail->send()) {
            echo "Email not sent. " , $mail->ErrorInfo , PHP_EOL;
        } else {
            echo "Email sent!" , PHP_EOL;
        }


    }
}
