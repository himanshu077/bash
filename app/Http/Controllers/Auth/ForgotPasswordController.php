<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use App\Transformers\Json;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
             $this->response(404, trans('passwords.user')) ;
            //return response()->json(Json::response(, ), 400);
        }

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
        return $response == Password::RESET_LINK_SENT
            ? $this->response(200, 'Reset link sent to your email.')
            : $this->response(404, 'Unable to send reset link.');


        //$token = $this->broker()->createToken($user);

        /*return response()->json([
            'success' => true,
            'token' => $token,
        ]);*/
    }




    public function __invoke(Request $request)
    {
        $this->validateEmail($request);
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
        return $response == Password::RESET_LINK_SENT
            ? $this->response(200, 'Reset link sent to your email.')
            : $this->response(404, 'Unable to send reset link.') ;
    }

    public function response($status,$status_message,$data=null) {
        $status_string = $status . ' ' . __($status_message);
        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, true, $status);
        $results = array();
        if(!empty($status_message)){
             //$results['mesg'] = __($status_message);
        }
        if(!empty($data)){
          $results['data'] = $data;
          $results['mesg'] = __($status_message);
        }else{
          $results['data'] =  new \stdClass(); 
          $results['mesg'] = __($status_message);
        }
        
        echo json_encode($results);
        die(0);
    }
	






}
