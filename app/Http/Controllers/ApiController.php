<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;

use RedisClient\RedisClient;
use RedisClient\Client\Version\RedisClient2x6;
use RedisClient\ClientFactory;

use App\User;
use App\Face;
use App\Bash;
use App\Crash;
use App\Follow;
use App\Ticket;
use App\Wallet;
use App\TempWallet;
use App\Qrcode;
use App\Checkin;
use App\Rating;
use App\SafeRide;
use App\Notify;
use App\LocationUpdate;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use JWTAuth;
use Mockery\Exception;
use Twilio\Exceptions\RestException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Redis;
use Keygen\Keygen;

use Illuminate\Support\Facades\View;
use PHPMailer\PHPMailer\PHPMailer;
use Twilio\Rest\Client;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Arr;

//use Illuminate\Http\Request;
use AWS;
use DB;

use Illuminate\Routing\UrlGenerator;

class ApiController extends Controller
{
    public $loginAfterSignUp = true;
    protected $url;
	public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }
	
    public function register(Request $request){
        if(!empty($_REQUEST['email'])){
            $rules = array(
                'email' => 'required|email|unique:users',
                //'username' => 'required|unique:users',
            );
    
            $messages = array(
                'email.unique' => 'This email is already registered, please choose another email',
            );
			
			$checkEmail = User::where('email', '=', $_REQUEST['email'])->first();
            
			if(!empty($checkEmail)){
				api_response(404,'This email is already registered, please choose another email');
			}	
			
        }
        
        if(!empty($_REQUEST['phone_number'])){
            $rules = array(
                'phone_number' => 'required|unique:users',
                //'username' => 'required|unique:users',
            );
    
            $messages = array(
                'phone_number.unique' => 'This phone number is already registered, please click ‘trouble logging in?’  on the first page.',
            );
        }
        
        $validator = Validator::make($request->all(), $rules, $messages);
        //print_r($validator); die;
        if ($validator->fails()) {

            $failedRules = $validator->failed();
            $errors = $validator->errors();
           
            if (isset($failedRules['email']['Unique'])) {
                $email_message = $errors->get("email");
                //print_r($email_message); die;
                api_response(404,$errors->first("email"));
            }
            if (isset($failedRules['username']['Unique'])) {
                $user_name = $errors->get("username");
                //print_r($user_name); die;
                api_response(404,$errors->first("username"));
            }

            if (isset($failedRules['phone_number'])) {
                $phone_number = $errors->get("phone_number");
                api_response(404,$errors->first("phone_number"));
            }
        }
       
       	
        try {
            $user = new User();
            $user->email = $request->email;
			$user->fname = $request->fname;
			$user->lname = $request->lname;
			$user->dob = $request->dob;
			$user->username = $request->username;
			$user->social_id = $request->social_id;
			$user->country_code = $request->country_code;
            $user->phone_number = $request->phone_number;
            $user->device_token = $request->deviceToken;
            $user->device_type = $request->deviceType;
            $user->userType = '1';
            if(!empty($request->gender)){
                $user->gender = $request->gender;
            }
			if(!empty($request->image)){
                $user->image = $request->image;
            }
            //$user->otp = rand(10000,99999);
			//$user->password = bcrypt($request->password);
            $user->session_id = Keygen::alphanum(16)->generate();
            if(!empty($_REQUEST['email'])){
                //$user->email_verify_token = Keygen::alphanum(64)->generate();
            }
            
            $user->save();

        }catch (\Illuminate\Database\QueryException $exception){
            $errorCode = $exception->errorInfo[1];
            api_response(404,$exception->getMessage());
        }catch(\Exception $exception){
            api_response(404,$exception->getMessage());
        }
       
        $customClaims = ['email' => $_REQUEST['email']];
        $jwt_token = JWTAuth::fromUser($user, $customClaims);

        api_response(200,'User registered in successfully',array('token'=> $jwt_token));
    }


    public function update_profile(Request $request){
        $token = $request->bearerToken();
        $rules = $messages = array();
        try {
            $user = JWTAuth::authenticate($token);
        } catch (JWTException $exception) {
            api_response(403,'Invalid Bearer Token for User');
        }
        //$user = User::findorfail($user->id);
        //print_r($user); die; 
        if(!empty($_REQUEST['email'])){
            $rules = array(
                'email' => 'required|email|unique:users,email,'.$user->id,
                //'username' => 'required|unique:users',
            );
    
            $messages = array(
                'email.unique' => 'This email is already registered, please choose another email',
            );
        }
        
        if(!empty($_REQUEST['phone_number'])){
            $rules = array(
                'phone_number' => 'required|unique:users,phone_number,'.$user->id,
            );
    
            $messages = array(
                'phone_number.unique' => 'This phone number is already registered, please click ‘trouble logging in?’  on the first page.',
            );
        }
        
        if(!empty($_REQUEST['username'])){
            $rules = array(
                'username' => 'required|unique:users,username,'.$user->id,
            );
    
            $messages = array(
                'username.unique' => 'This username is already registered, please choose another username',
            );
        }

        

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {

            $failedRules = $validator->failed();
            $errors = $validator->errors();
            
            if (isset($failedRules['email']['Unique'])) {
                $email_message = $errors->get("email");
                //print_r($email_message); die;
                api_response(404,$errors->first("email"));
            }
            if (isset($failedRules['username']['Unique'])) {
                $user_name = $errors->get("username");
                //print_r($user_name); die;
                api_response(404,$errors->first("username"));
            }

            if (isset($failedRules['phone_number'])) {
                $phone_number = $errors->get("phone_number");
                api_response(404,$errors->first("phone_number"));
            }
        }

        try {
            if(!empty($request->email)){
                $user->email = $request->email;
            }

            if(!empty($request->fname)){
                $user->fname = $request->fname;
            }

            if(!empty($request->linked_snap)){
                $user->linked_snap = $request->linked_snap;
            }

            if(!empty($request->lname)){
                $user->lname = $request->lname;
            }
            if(!empty($request->dob)){
                $user->dob = $request->dob;
            }
			
            if(!empty($request->username)){
                $user->username = $request->username;
            }
			if(!empty($request->country_code)){
                $user->country_code = $request->country_code;
            }
            if(!empty($request->phone_number)){
                $user->phone_number = $request->phone_number;
            }

            if(!empty($request->gender)){
                $user->gender = $request->gender;
            }

            if(!empty($request->address)){
                $user->address = $request->address;
            }

            if(!empty($request->image)){
                $user->image = $request->image;
            }
            
            if(!empty($request->lat)){
                $user->lat = $request->lat;
            }

            if(!empty($request->lng)){
                $user->lng = $request->lng;
            }

            if(!empty($request->costume_id)){
                $user->costume_id = $request->costume_id;
            }

            if(!empty($request->avtar_img)){
                $user->avtar_img = $request->avtar_img;
            }
            
            if(!empty($_FILES['standing_img'])){

                if(isset($_FILES['standing_img']) && $_FILES['standing_img']['name'] !== '' && !empty($_FILES['standing_img']['name'])){
                    $file = $_FILES['standing_img'];
                    $file = preg_replace("/[^a-zA-Z0-9.]/", "", $file['name']);
                    $filename = time().'-'.$file;
                    $ext = substr(strtolower(strrchr($file, '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions

                    if(in_array($ext, $arr_ext))
                    {
                    $path="public/images/standing_img/";
                    if(move_uploaded_file($_FILES['standing_img']['tmp_name'],$path.$filename)){
                        $user->standing_img = $path.$filename;
                    }
                    }else{
                        api_response(404,'Please upload valid image type');
                    }
				}
            }

            $user->save();

        }catch (\Illuminate\Database\QueryException $exception){
            $errorCode = $exception->errorInfo[1];
            api_response(404,$exception->getMessage());
        }catch(\Exception $exception){
            api_response(404,$exception->getMessage());
        }
        
        $user_data = $this->get_user($user->id,'','1');
        api_response(200,'User updated successfully',$user_data);
    }
    
    public function update_paypal(Request $request){
        $token = $request->bearerToken();
        $rules = $messages = array();
        try {
            $user = JWTAuth::authenticate($token);
        } catch (JWTException $exception) {
            api_response(403,'Invalid Bearer Token for User');
        }
        

        try {
            //if(!empty($request->paypal_id)){
                $user->paypal_id = $request->paypal_id;
            //}

            //if(!empty($request->paypal_name)){
                $user->paypal_name = $request->paypal_name;
            //}

            $user->save();

        }catch (\Illuminate\Database\QueryException $exception){
            $errorCode = $exception->errorInfo[1];
            api_response(404,$exception->getMessage());
        }catch(\Exception $exception){
            api_response(404,$exception->getMessage());
        }
        
        $user_data = $this->get_user($user->id,'','1');
        api_response(200,'Paypal details updated successfully',$user_data);
    }

    public function api_social_login(Request $request){
        
         //$result = "response of api_social_login ";
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true)); 
		
        $user = array();
        try{
            $user = User::where('social_id', '=', $_REQUEST['social_id'])->firstOrFail();
        }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
            api_response(200,'User logged in successfully');

            $rules = $messages = array();
            if(!empty($_REQUEST['email'])){
                $rules = array(
                    'email' => 'required|email|unique:users',
                    'username' => 'required|unique:users',
                );
        
                $messages = array(
                    'email.unique' => 'This email is already registered, please choose another email',
                );
            }
            
            if(!empty($_REQUEST['phone_number'])){
                $rules = array(
                    'phone_number' => 'required|unique:users',
                    'username' => 'required|unique:users',
                );
        
                $messages = array(
                    'phone_number.unique' => 'This phone number is already registered, please click ‘trouble logging in?’  on the first page.',
                );
            }
            
            $validator = Validator::make(Input::all(), $rules, $messages);

            if ($validator->fails()) {

                $failedRules = $validator->failed();
                $errors = $validator->errors();
                
                if (isset($failedRules['email']['Unique'])) {
                    $email_message = $errors->get("email");
                    //print_r($email_message); die;
                    api_response(404,$errors->first("email"));
                }
                if (isset($failedRules['username']['Unique'])) {
                    $user_name = $errors->get("username");
                    //print_r($user_name); die;
                    api_response(404,$errors->first("username"));
                }

                if (isset($failedRules['phone_number'])) {
                    $phone_number = $errors->get("phone_number");
                    api_response(404,$errors->first("phone_number"));
                }
            }
            $new_username_d = '';
            if(!empty($user)){
                
            }else{
               // api_response(200,'User logged in successfully');
                if(!empty($request->fname)){
                    $username = $request->fname;
                    $username = preg_replace("/[^A-Za-z0-9]/", "", $username);
                    $username = strtolower($username);
                    for( $i = 0; $i<20; $i++ ) {
                        $new_username = $username . Keygen::numeric(2)->generate();
                        $new_count =  User::where('username', 'like', "%$new_username%")->count();
                        if($new_count <= 0){
                            $new_username_d = $new_username;
                            //api_response(404,'username taken', array('suggestion' => $new_username));
                        }else{
                            exit;
                        }
                    }
                }
            }
            
            $user = new User();
            //return api_response(404,'Token not found...');
        }
        //print_r($user); die;
        try {
            if(!empty($request->email)){
                $user->email = $request->email;
            }

            $user->social_id = $request->social_id;

            if($request->social_type == '1'){
                $user->linked_fb = '1';
            }else{
                $user->linked_snap = '1';
            }
            if(!empty($request->fname)){
                $user->fname = $request->fname;
            }
            if(!empty($request->lname)){
                $user->lname = $request->lname;
            }
            if(!empty($request->dob)){
                $user->dob = $request->dob;
            }
			
            if(!empty($new_username_d)){
                $user->username = $new_username_d;
            }
			if(!empty($request->country_code)){
                $user->country_code = $request->country_code;
            }
            if(!empty($request->phone_number)){
                $user->phone_number = $request->phone_number;
            }
            
            $user->userType = '1';
            //$user->otp = rand(10000,99999);
            //$user->password = bcrypt($request->password);
            if(!empty($user->session_id)){

            }else{
                $user->session_id = Keygen::alphanum(16)->generate();
            }
            
            if(!empty($_REQUEST['email'])){
                //$user->email_verify_token = Keygen::alphanum(64)->generate();
            }
            
            $user->save();

        }catch (\Illuminate\Database\QueryException $exception){
            $errorCode = $exception->errorInfo[1];
            api_response(404,$exception->getMessage());
        }catch(\Exception $exception){
            api_response(404,$exception->getMessage());
        }
        if(!empty($_REQUEST['email'])){
            //$email_result = $this->send_verify_email($user->email, $user->email_verify_token);
        }
        
        $customClaims = ['social_id' => $_REQUEST['social_id']];
        $jwt_token = JWTAuth::fromUser($user, $customClaims);
         // $result = "response of api_social_login ";
		//$result .= json_encode($jwt_token);
		//\Log::info(print_r($result, true)); 
        api_response(200,'User logged in successfully',array('token'=> $jwt_token));
    }


    public function login(Request $request){

        $jwt_token = false;
        
        $original_input = $request->only('email', 'password');

        $customClaims = ['email' => $original_input['email'], 'password' => $_REQUEST['password']];
        $jwt_token =  JWTAuth::attempt($original_input, $customClaims);

        if($jwt_token == false){
            $new_input = array('phone_number' => $original_input['email'], 'password' => $original_input['password']);
            $customClaims = ['phone_number' => $new_input['phone_number'], 'password' => $new_input['password']];
            $jwt_token =  JWTAuth::attempt($new_input, $customClaims);
        }

        if($jwt_token == false){
            $new_input = array('username' => $original_input['email'], 'password' => $original_input['password']);
            $customClaims = ['username' => $new_input['username'], 'password' => $new_input['password']];
            $jwt_token =  JWTAuth::attempt($new_input, $customClaims);
        }


        if ($jwt_token == false) {
            api_response(404,'Please enter valid login details');
        }
        
        $data = array('token'=> $jwt_token);
        api_response(200,'User logged in successfully',$data);
    }

    public function verify_email_do(Request $request){

        if(!empty($_REQUEST['email'])  && !empty($_REQUEST['token'])){

            try{
                $user = User::where('token', '=', $_REQUEST['token'])->firstOrFail();
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                return api_response(404,'Token not found...');
            }
                $user->email_verified = '1';
                $user->save();
                api_response(200,'Email verified successfully');
        }else{
            return api_response(400,'Wrong data entered');
        }
    }


    public function send_otp(Request $request){
        //print_r(env('AWS_FROM_EMAIL'));
        if(!empty($_REQUEST['phone']) || !empty($_REQUEST['email'])){
            if($_REQUEST['type'] == '1'){
                if(!empty($_REQUEST['email'])){
                    try{
                        $user = User::where('email', '=', $_REQUEST['email'])->firstOrFail();
                    }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                        api_response(200,'You are not registered with this email',array('is_registered'=>'0'));
                    } 
                  }  
                  
                  if(!empty($_REQUEST['phone'])){
                      
                    try{
                        $user = User::where('phone_number', '=', $_REQUEST['phone'])->firstOrFail();
                    }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                         api_response(200,'You are not registered with this phone number',array('is_registered'=>'0'));
                    } 
                  } 
            }else{
                if(!empty($_REQUEST['email'])){
                    try{
                        $user = User::where('email', '=', $_REQUEST['email'])->firstOrFail();
                        if(!empty($user)){
                            api_response(404,'This email is already registered, please choose another email');
                        }
                    }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                        //api_response(404,'You are not registered with this email');
                    } 
                  }  
                  
                  if(!empty($_REQUEST['phone'])){
                    try{
                        $user = User::where('phone_number', '=', $_REQUEST['phone'])->firstOrFail();
                        if(!empty($user)){
                            api_response(404,'This phone number is already registered, please click ‘trouble logging in?’  on the first page.');
                        }
                    }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                        //api_response(404,'You are not registered with this phone number');
                    } 
                  } 
            }
            
            $phone = $_REQUEST['country_code'].$_REQUEST['phone'];
            $email = $_REQUEST['email'];
            $otp_code = rand(100000,999999);
            if(!empty($phone)){
                
                $message = 'Hi your BASH verification code is '. $otp_code; 
                $this->sendSMS($phone, $message);
            }
            
            if(!empty($email)){
                //$otp_code = rand(100000,999999);
                
                $this->send_verify_email_otp($email, $otp_code);
            }

		  if($_REQUEST['type'] == '1'){
				api_response(200,'Verification code has been send successfully',array('otp'=> $otp_code,'is_registered'=>'1'));
			}else{
				api_response(200,'Verification code has been send successfully',array('otp'=> $otp_code));
			}
        }else{
            return api_response(400,'Please fill all required data');
        }
    }


    public function reset_password(Request $request){

        if((!empty($_REQUEST['email']) || !empty($_REQUEST['phone']))  && !empty($_REQUEST['password']) ){
              
          if(!empty($_REQUEST['email'])){
            try{
                $user = User::where('email', '=', $_REQUEST['email'])->firstOrFail();
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'You are not registered with this email');
            } 
          }  
          
          if(!empty($_REQUEST['phone'])){
            try{
                $user = User::where('phone_number', '=', $_REQUEST['phone'])->firstOrFail();
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'You are not registered with this phone number');
            } 
          } 

          $user->password = $user->password = bcrypt($request->password);;
          $user->save();
          api_response(200,'Password updated successfully');
        }else{
            api_response(404,'Please fill all required data');
        }
    }


    public function check_username(Request $request){
        $username = $request->username;
        //print_r($username); die;
        $count = User::where('username', 'like', "%$username%")->count();
        //print_r($count); die;
        if($count <= 0)
        {
            api_response(200,'username available',array('response'=>'1'));
        }
        else
        {
            api_response(200,'username unavailable',array('response'=>'0'));
        }
    }


    public function suggest_username(Request $request){
        //echo date("d-m-Y H:i:s"); die;
        $username = trim($request->name);
        $username = preg_replace("/[^A-Za-z0-9]/", "", $username);
        $username = strtolower($username);
        $new_username = '';
        $new_username_d = array();
            for( $i = 0; $i<4; $i++ ) {
                if(!empty($new_username)){
                    $new_username = $username . Keygen::numeric(2)->generate();
                }else{
                    $new_username = $username;
                }
                
                $new_count =  User::where('username', 'like', "%$new_username%")->count();
                if($new_count <= 0){
                    if(!empty($new_username_d)){
                        if(in_array($new_username,$new_username_d)){
                            $i--;
                            continue;
                        }else{
                            $new_username_d[] = array('name'=> $new_username);
                        }
                    }else{
                        $new_username_d[] = array('name'=> $new_username);
                    }
                }else{
                    $i--;
                    continue;
                }
            }
            api_response(200,'username taken', $new_username_d);
    }

    
    public function update_token(Request $request){

        if(!empty($_REQUEST['user_id'])){
            try{
                $user = User::findorfail($_REQUEST['user_id']);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(401,'Please enter valid user id');
            } 

          
            if(!empty($_FILES['image'])){
				
				if(isset($_FILES['image']) && $_FILES['image']['name'] !== '' && !empty($_FILES['image']['name'])){
					$file = $_FILES['image'];
					$file = preg_replace("/[^a-zA-Z0-9.]/", "", $file['name']);
					$filename = time().'-'.$file;
					//$ext = substr(strtolower(strrchr($file, '.')), 1); //get the extension
					//$arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
					 $path = "public/images/user_avtar/";
					 //$real_path = public_path()."/images/user_avtar/";
					if($file = move_uploaded_file($_FILES['image']['tmp_name'],$path.$filename)){
						$user->image =  $path.$filename;
					} 
				}
			}else if(!empty($_REQUEST['image'])){
			    $user->image = $_REQUEST['image'];
            } 
          $user->device_token = $request->device_token;
          $user->device_type = $request->device_type;
		  User::where('id', '!=', $_REQUEST['user_id'])->where('device_token', '=', $_REQUEST['device_token'])->where('device_type', '=', $_REQUEST['device_type'])->update(array('device_token' => '','device_type' => ''));
          $user->save();

            api_response(200,'Token updated successfully',$this->get_user($user->id,'','1'));
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
	public function update_location(Request $request){

        if(!empty($_REQUEST['user_id'])){
            try{
                $user = User::findorfail($_REQUEST['user_id']);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(401,'Please enter valid user id');
            } 

           if(isset($request->location)){
			   
			   if($request->location == '2'){
				   $user->block_users = $request->block_users;
			   }else{
				   $user->location = $request->location;
			   }
		   }
          
		   $user->save();

            api_response(200,'Token updated successfully',$this->get_user($user->id,'','1'));
        }else{
            api_response(404,'Please fill all required data');
        }
    }
	
    public function update_prefrence(Request $request){

        if(!empty($_REQUEST['user_id']) && isset($_REQUEST['bash_type']) && isset($_REQUEST['distance'])){
            try{
                $user = User::findorfail($_REQUEST['user_id']);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(401,'Please enter valid user id');
            } 

          
          
          $user->pref_bash_type = $request->bash_type;
          $user->pref_distance = $request->distance;
          $user->save();

            api_response(200,'Data updated successfully',$this->get_user($user->id,'','1'));
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function get_user_data(Request $request){

        if(!empty($_REQUEST['user_id']) ){
            $user_data = $this->get_user1($_REQUEST['user_id'],$_REQUEST['other_id'],'1');
			$result = "response  ";
	    	//$result .= json_encode($user_data);
		    //\Log::info(print_r($result, true));
            api_response(200,'Data listed successfully',$user_data);
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function get_token(Request $request){

        if(!empty($_REQUEST['type'])  && !empty($_REQUEST['value']) ){
          if($_REQUEST['type'] == '1'){
            try{
                $user = User::where('email', '=', $_REQUEST['value'])->firstOrFail();
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'You are not registered with this email');
            } 
            $customClaims = ['email' => $_REQUEST['value']];
            $jwt_token = JWTAuth::fromUser($user, $customClaims);

          }else{
            try{
                $user = User::where('phone_number', '=', $_REQUEST['value'])->firstOrFail();
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'You are not registered with this email');
            }   
            $customClaims = ['phone_number' => $_REQUEST['value']];
            $jwt_token = JWTAuth::fromUser($user, $customClaims);
          }
          
            api_response(200,'Data listed successfully',array('token'=>$jwt_token));
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function logout(Request $request){
        
        $token = $request->bearerToken();
              
        try {
			$result = "logout";
			//$result .= $token;
		    //\Log::info(print_r($result, true));
            JWTAuth::invalidate($token);
            api_response(200,'User logged out successfully');
        } catch (JWTException $exception) {
			$result = "not logout";
		    //\Log::info(print_r($result, true));
            api_response(401,'Sorry, the user cannot be logged out');
        }catch (\Illuminate\Database\QueryException $exception){
            api_response(401,'Token is Invalid');
        }catch(\Exception $exception){
            api_response(401,'Token is Invalid');
        }
    }
    

    public function getAuthUser(Request $request){
        //echo $result = "request of ping "; die;
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true));
		
        $token = $request->bearerToken();

        try {
            $user = JWTAuth::authenticate($token); 
            //print_r($user); die;
            $user_data = $this->get_user($user->toArray()['id'],'','1');
			$result = "response of ping ";
		    //$result .= json_encode($user_data);
	    	//\Log::info(print_r($result, true));
		
            api_response(200,'User listed successfully',$this->nullvalue_filter($user_data));
        } catch (JWTException $exception) {
            api_response(403,'Invalid Bearer Token for User');
        }catch (\Illuminate\Database\QueryException $exception){
            api_response(401,'Token is Invalid');
        }catch(\Exception $exception){
            api_response(401,'Token is Invalid');
        }
    }

    public function create_bash(Request $request){
        // bash_type= Restaurant - 1 Club - 2 Bar -3
        // is private(0=public/1=private)
        $result = "request of create bash ";
		    //$result .= json_encode($_REQUEST);
	    	//\Log::info(print_r($result, true));
			
        try {
            $start = $request->start_date;
            $start_time = $request->start_date.' '.$request->start_time;
            $end = $request->end_date;
            $end_time = $request->end_date.' '.$request->end_time;
            $loc = $request->location;
			$lat = $request->lat;
			$lon = $request->lng;
            $check_duplicate = Bash::select("bashes.*"
                    ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                    * cos(radians(bashes.lat)) 
                    * cos(radians(bashes.lng) - radians(" . $lon . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(bashes.lat))) AS distance"))
				->where(function ($query) use ($start,$start_time,$end,$end_time,$loc) {
                    $query->where('start_date_time', '<=', $end_time)
                      ->where('end_date_time', '>=', $end_time);
                      //->where('location', '=', $loc);
                })->orWhere(function ($query) use ($start,$start_time,$end,$end_time,$loc) {
                    $query->where('start_date_time', '<=', $start_time)
                      ->where('end_date_time', '>=', $start_time);
                      //->where('location', '=', $loc);
            })->orderBy('distance','ASC')->first();
			//echo $check_duplicate->id;
			//echo $check_duplicate->distance;
            $distance_check = $check_duplicate->distance*1609.34;
            if(!empty($check_duplicate) && $distance_check >= '0' && $distance_check <= '5'){
                api_response(404,'This location is already booked for this timeframe.');
            }
            $bash = new Bash();
            $bash->user_id = $request->user_id;
			$bash->bash_type = $request->bash_type;
			$bash->name = $request->name;
			$bash->name_of_host = $request->name_of_host;
			$bash->start_date = $request->start_date;
			$bash->start_time = $request->start_time;
			$bash->end_time = $request->end_time;
            $bash->location = $request->location;
            $bash->lat = $request->lat;
            $bash->lng = $request->lng;
            $bash->charge = $request->charge;
            $bash->age = $request->age;
            $bash->age_max = $request->age_max;
            $bash->end_date = $request->end_date;
            $bash->end_date_time = $request->end_date.' '.$request->end_time;
            $bash->start_date_time = $request->start_date.' '.$request->start_time;
            $bash->is_private = $request->is_private;

            if(!empty($request->timezone)){
                $bash->timezone = $request->timezone;
            }
            if(!empty($request->description)){
                $bash->description = $request->description;
            }
			
            $bash->session_id = Keygen::alphanum(16)->generate();
            $bash->save();

        }catch (\Illuminate\Database\QueryException $exception){
            $errorCode = $exception->errorInfo[1];
            api_response(404,$exception->getMessage());
        }catch(\Exception $exception){
            api_response(404,$exception->getMessage());
        }
		 $result = "responce of create bash ";
		    //$result .= json_encode(array('id'=> $bash->id,'count'=>$this->get_crash_count_today($request->user_id)));
	    	//\Log::info(print_r($result, true));
        api_response(200,'Bash has been created successfully',array('id'=> $bash->id,'count'=>$this->get_crash_count_today($request->user_id)));
    }

    public function update_bash(Request $request){
        // bash_type= Restaurant - 1 Club - 2 Bar -3
        // is private(0=public/1=private)
        if(!empty($_REQUEST['bash_id'])){
            try {
                
                $bash = Bash::findorfail($_REQUEST['bash_id']);
                
                $start = $request->start_date;
                $start_time = $request->start_date.' '.$request->start_time;
                $end = $request->end_date;
                $end_time = $request->end_date.' '.$request->end_time;
                $loc = $request->location;
                $id = $_REQUEST['bash_id'];
			    $lat = $request->lat;
			    $lon = $request->lng;
                $check_duplicate = Bash::select("bashes.*"
                    ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                    * cos(radians(bashes.lat)) 
                    * cos(radians(bashes.lng) - radians(" . $lon . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(bashes.lat))) AS distance"))
				->where(function ($query) use ($start,$start_time,$end,$end_time,$loc,$id) {
                    $query->where('start_date_time', '<=', $end_time)
                        ->where('end_date_time', '>=', $end_time)
                        ->where('id', '!=', $id);
                        //->where('location', '=', $loc);
                })->orWhere(function ($query) use ($start,$start_time,$end,$end_time,$loc,$id) {
                    $query->where('start_date_time', '<=', $start_time)
                        ->where('end_date_time', '>=', $start_time)
                        ->where('id', '!=', $id);
                       // ->where('location', '=', $loc);
                })->orderBy('distance','ASC')->first();
            //print_r($check_duplicate); die;
			    $distance_check = $check_duplicate->distance*1609.34;
				if(!empty($check_duplicate) && $distance_check >= '0' && $distance_check <= '5'){
                    api_response(404,'This location is already booked for this timeframe.');
                }

                $bash->is_private = $request->is_private;
                

                if(!empty($request->user_id)){
                    $bash->user_id = $request->user_id;
                }
                if(!empty($request->bash_type)){
                    $bash->bash_type = $request->bash_type;
                }
                if(!empty($request->name)){
                    $bash->name = $request->name;
                }
                if(!empty($request->name_of_host)){
                    $bash->name_of_host = $request->name_of_host;
                }
                if(!empty($request->start_date)){
                    $bash->start_date = $request->start_date;
                }
                if(!empty($request->start_time)){
                    $bash->start_time = $request->start_time;
                }
                if(!empty($request->end_time)){
                    $bash->end_time = $request->end_time;
                }
                if(!empty($request->location)){
                    $bash->location = $request->location;
                }
                if(!empty($request->lat)){
                    $bash->lat = $request->lat;
                }
                if(!empty($request->lng)){
                    $bash->lng = $request->lng;
                }
                if(!empty($request->charge)){
                    $bash->charge = $request->charge;
                }
                if(!empty($request->age)){
                    $bash->age = $request->age;
                }
                $bash->age_max = $request->age_max;
                if(!empty($request->end_date)){
                    $bash->end_date = $request->end_date;
                }

                if(!empty($request->end_date) && !empty($request->end_time) ){
                    $bash->end_date_time = $request->end_date.' '.$request->end_time;
                }

                if(!empty($request->start_date) && !empty($request->start_time) ){
                    $bash->start_date_time = $request->start_date.' '.$request->start_time;
                }

                if(!empty($request->description)){
                    $bash->description = $request->description;
                }
			
                //if(!empty($request->end_date_time)){
                   // $bash->end_date_time = $request->end_date.' '.$request->end_time;
                   // $bash->start_date_time = $request->start_date.' '.$request->start_time;
                //}
                $bash->save();
				    $user_data = User::where('id', '=', $_REQUEST['user_id'])->first();
					$bash_data = Bash::where('id', '=', $id)->first();
					
				    $tiket_datas = TempWallet::where('bash_id', '=', $bash->id)->get()->toarray();
					if(!empty($tiket_datas)){
						foreach($tiket_datas as $tiket_da){
							$mes = $user_data->fname.' '.$user_data->lname." has changed event details for ".$bash_data->name.". Please review these changes.";
							$this->save_notify($tiket_da['from_id'],'Event updated', $mes,'update_event',$id);
						}
					}
				 
				    // notify free member
					$free_mambers = Crash::whereHas('bash', function ($query) use ($id) {
						 $query->where('bash_id','=', $id);
					})->get()->toarray();
					//print_r($free_mambers); die;
					foreach($free_mambers as $free_mambe){
						
						$mes = $user_data->fname.' '.$user_data->lname." has changed event details for ".$bash_data->name.". Please review these changes.";
						$this->save_notify($free_mambe['user_id'],'Event updated', $mes,'update_event',$id);
					}
					
                api_response(200,'Bash has been updated successfully',array('id'=> $bash->id,'count'=>$this->get_crash_count_today($request->user_id)));
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
            
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function delete_bash(){
        if(!empty($_REQUEST['bash_id'])){
            try{
                $bash_data = Bash::where('id', '=', $_REQUEST['bash_id'])->first();
                $user_data = User::where('id', '=', $_REQUEST['user_id'])->first();
						
                $tiket_datas = TempWallet::where('bash_id', '=', $_REQUEST['bash_id'])->get()->toarray();
				$wallet_deduct = 0;
                if(!empty($tiket_datas)){
                    foreach($tiket_datas as $tiket_da){
                        
                        $mes = $user_data->fname.' '.$user_data->lname." has deleted ".$bash_data->name.". Refund has been issued to your account.";
                        $this->save_notify($tiket_da['from_id'],'Event deleted', $mes,'delete_event');
                       
                        $this->add_wallet($tiket_da['from_id'],$tiket_da['total_paid'],'credit',$tiket_da['bash_id']);
                        TempWallet::where('id','=',$tiket_da['id'])->delete();
						$wallet_deduct += $tiket_da['tax'];
                    }
				 }
				 $this->add_wallet($bash_data->user_id,$wallet_deduct,'debit',null,'','','');
				  // notify free member
					$free_mambers = Crash::whereHas('bash', function ($query) {
						 $query->where('bash_id','=', $_REQUEST['bash_id']);
					})->get()->toarray();
					$mes = $user_data->fname.' '.$user_data->lname." has deleted ".$bash_data->name.".";
					foreach($free_mambers as $free_mambe){
						$this->save_notify($free_mambe['user_id'],'Event deleted', $mes,'delete_event');
					}
					
                Bash::where('id','=',$_REQUEST['bash_id'] )->delete();
                api_response(200,'Bash has been deleted successfully');
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }
        }else{
            api_response(200,'Please fill all required data');
        }
    }

    public function bash_list(){
		$result = "request  of bash list ";
		 //$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true));
		
        $q = Bash::query();
        
        //echo date('Y-m-d H:i:s');die;
        if(!empty($_REQUEST['date'])){
            $q->where('start_date','<=', $_REQUEST['date'] );
            $q->where('end_date','>=',$_REQUEST['date']);
            
        }else{
            
            $q->where('start_date','<=', date('Y-m-d'));
            $q->where('end_date_time','>=', date('Y-m-d H:i:s'));
            
        }
        if(!empty($_REQUEST['txt'])){
            $search_text = $_REQUEST['txt'];
            $q->where('name','like','%'. $search_text .'%');
            
        }
        

        if(!empty($_REQUEST['user_id'])){
            $user_data = $this->get_user($_REQUEST['user_id']);
            $diff = 0;
			
			if($user_data['dob'] == '0000-00-00'){
			}else{
			  $diff = (date('Y') - date('Y',strtotime($user_data['dob'])));
			}
            // if(!empty($user_data['pref_bash_type'])){
			// 	$pref_bash_type = count(explode(',',$user_data['pref_bash_type']));
			// 	if($pref_bash_type == 1 ){
			// 		$bsh_key = explode(',',$user_data['pref_bash_type']);
			// 		//print_r($bsh_key[0]);
			// 		$q->where(function ($query) use ($bsh_key) {
			// 			$query->where('bash_type', '=', $bsh_key[0]);
			// 		});
			// 	}
			// 	if($pref_bash_type == 2 ){
			// 		$bsh_key = explode(',',$user_data['pref_bash_type']);
			// 		$q->where(function ($query) use ($bsh_key){
			// 			$query->where('bash_type', '=', $bsh_key[0])->orWhere('bash_type', '=', $bsh_key[1]);
			// 		});
			// 	}
			// 	if($pref_bash_type == 3 ){
			// 		$bsh_key = explode(',',$user_data['pref_bash_type']);
			// 		$q->where(function ($query) use ($bsh_key){
			// 			$query->where('bash_type', '=', $bsh_key[0])->orWhere('bash_type', '=', $bsh_key[1])->orWhere('bash_type', '=', $bsh_key[2]);
			// 		});
			// 	}
            // }

            $usr = $_REQUEST['user_id'];
        }
        $results = $q->orderBy('start_date_time','ASC')->get()->toArray();
        
        //$results = $q->orderBy('id','DESC')->toSql();
        //print_r($results); die;
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){ 
                
                $data = $this->get_bash_long($res['id'],$_REQUEST['user_id']);
                if(!empty($data)){
                    //print_r($data['name_of_host']); 
                    // get age
					  
							  
                    if($user_data){
						
                        /*if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                            //print_r($res); 
                        }else{
                            if(!empty($user_data['pref_bash_type'])){
                                if(in_array($data['bash_type'], explode(',',$user_data['pref_bash_type']))){

                                }else{
                                    continue;
                                }
                            }
                        } */
                        // date check
						//echo $diff; 
						
							if($data['is_private'] == '1'){
								if(in_array($_REQUEST['user_id'], explode(',', $res['name_of_host'])) || in_array($_REQUEST['user_id'], explode(',', $res['private_users'])) || $_REQUEST['user_id']  == $res['user_id']){
									if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
										
											$finalData[] = $data;
										
										
									}else{
										$finalData[] = 	$data;
										
										/*if($user_data['pref_distance'] >= $data['distance'] ){
											if(!empty($res['age_max'])){
												
											
												if($diff >= $res['age'] && $diff <= $res['age_max']){
													$finalData[] = 	$data;
												}
												
											}else{
												if($diff >= $res['age']){
													$finalData[] = 	$data;
												}
											}
											
										}*/
									}

									// if($user_data['pref_distance'] >= $data['distance'] ){
									//     $finalData[] = $data;
									// }
								}
							}else{
								
								if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
									
											$finalData[] = $data;
										
								}else{
									
									if(!empty($user_data['pref_bash_type'])){
										if(in_array($data['bash_type'], explode(',',$user_data['pref_bash_type']))){

										}else{
											continue;
										}
									}
									if($user_data['pref_distance'] >= $data['distance'] ){
										if(!empty($res['age_max'])){
											
												if($diff >= $res['age'] && $diff <= $res['age_max']){
													
													$finalData[] = 	$data;
												}
												
										}else{
											if($diff >= $res['age']){
												$finalData[] = 	$data;
											}
										}
										
									}
								}
							}
						
                        
                        
                    }else{
                        if($data['is_private'] == '1'){
                            if(in_array($_REQUEST['user_id'], explode(',', $res['name_of_host'])) || in_array($_REQUEST['user_id'], explode(',', $res['private_users'])) || $_REQUEST['user_id']  == $res['user_id']){
                                $finalData[] = $data;
                            }
                        }else{
                            $finalData[] = $data;
                        }
                    }
                }
            }
        }
/* foreach ($data as $key => $row) {
    $distance[$key]  = $row['distance'];
    $ratings[$key] = $row['ratings'];
    $complement[$key] = $row['complement'];
    $nofpeople[$key] = $row['male']+$row['fe_male']+$row['not_specified'];
} */
// as of PHP 5.5.0 you can use array_column() instead of the above code
$distance  = array_column($finalData, 'distance');
//$ratings = array_column($finalData, 'ratings');
//$complement = array_column($finalData, 'complement');
//$nofpeople = array_column($finalData, 'male')+array_column($finalData, 'fe_male')+array_column($finalData, 'not_specified');

if(isset($_REQUEST['order']) && $_REQUEST['order']=='asc'){
	array_multisort($distance, SORT_ASC, $finalData); 
}else{
	array_multisort($distance, SORT_ASC, $finalData); 
}
        
        if(!empty($finalData)){
			     $result = "response  of bash list ";
		         //$result .= json_encode($finalData);
		       // \Log::info(print_r($result, true));
				
            api_response(200,'Bash listed successfully',$finalData);
        }else{
            api_response(200,'No data found','','array');
        }
    }
    

    public function calender_list(){
        
        $q = Bash::query();
        $q1 = Bash::query();
        //echo date('Y-m-d H:i:s');die;
        if(!empty($_REQUEST['date'])){
            $q->where('start_date','<=', $_REQUEST['date'] );
            $q->where('end_date','>=',$_REQUEST['date']);
            $q1->where('start_date','<=', $_REQUEST['date'] );
            $q1->where('end_date','>=',$_REQUEST['date']);
        }else{
            if($_REQUEST['today'] == '1'){
                $q->where('start_date','<=', date('Y-m-d'));
				$q->where('end_date_time','>=',date('Y-m-d H:i:s'));
                if(!empty($_REQUEST['user_id'])){
                   // $q->where('user_id','!=',$_REQUEST['user_id']);
                }
            }
            $q1->where('start_date','>', date('Y-m-d'));
            //$q->where('end_date_time','>=', date('Y-m-d H:i:s'));
            
        }

        if(!empty($_REQUEST['txt'])){
            $search_text = $_REQUEST['txt'];
            $q->where('name','like','%'. $search_text .'%');
            $q1->where('name','like','%'. $search_text .'%');
        }
        

        if(!empty($_REQUEST['user_id'])){
            $user_data = $this->get_user($_REQUEST['user_id']);
			$diff = 0;
			if($user_data['dob'] == '0000-00-00'){
			}else{
			  $diff = (date('Y') - date('Y',strtotime($user_data['dob'])));
			}
			//$q->where('age','<=',$diff);
			//$q1->where('age','<=',$diff);
			
            // if(!empty($user_data['pref_bash_type'])){
            //         $pref_bash_type = count(explode(',',$user_data['pref_bash_type']));
            //         if($pref_bash_type == 1 ){
            //             $bsh_key = explode(',',$user_data['pref_bash_type']);
            //             //print_r($bsh_key[0]);
            //             $q->where(function ($query) use ($bsh_key) {
            //                 $query->where('bash_type', '=', $bsh_key[0]);
            //             });
            //             $q1->where(function ($query) use ($bsh_key) {
            //                 $query->where('bash_type', '=', $bsh_key[0]);
            //             });
            //         }
            //         if($pref_bash_type == 2 ){
            //             $bsh_key = explode(',',$user_data['pref_bash_type']);
            //             $q->where(function ($query) use ($bsh_key){
            //                 $query->where('bash_type', '=', $bsh_key[0])->orWhere('bash_type', '=', $bsh_key[1]);
            //             });
            //             $q1->where(function ($query) use ($bsh_key){
            //                 $query->where('bash_type', '=', $bsh_key[0])->orWhere('bash_type', '=', $bsh_key[1]);
            //             });
            //         }
            //         if($pref_bash_type == 3 ){
            //             $bsh_key = explode(',',$user_data['pref_bash_type']);
            //             $q->where(function ($query) use ($bsh_key){
            //                 $query->where('bash_type', '=', $bsh_key[0])->orWhere('bash_type', '=', $bsh_key[1])->orWhere('bash_type', '=', $bsh_key[2]);
            //             });
            //             $q1->where(function ($query) use ($bsh_key){
            //                 $query->where('bash_type', '=', $bsh_key[0])->orWhere('bash_type', '=', $bsh_key[1])->orWhere('bash_type', '=', $bsh_key[2]);
            //             });
            //         }
            // }
            $usr = $_REQUEST['user_id'];
        }
        $results = $q->orderBy('start_date_time','ASC')->get()->toArray();
        $results1 = $q1->orderBy('start_date_time','ASC')->get()->toArray();
        //$results = $q->orderBy('id','DESC')->toSql();
        //print_r($results1); die;
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){ //print_r($res); die;
                
                $data = $this->get_bash_long($res['id'],$_REQUEST['user_id']);
                if(!empty($data)){
                   
                    /* if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                         
                    }else{
                        if(!empty($user_data['pref_bash_type'])){
                            if(in_array($data['bash_type'], explode(',',$user_data['pref_bash_type']))){

                            }else{
                                continue;
                            }
                        }
                    } */
                    
                    if($user_data){
                        if($data['is_private'] == '1'){
                            if(in_array($_REQUEST['user_id'], explode(',', $res['name_of_host'])) || in_array($_REQUEST['user_id'], explode(',', $res['private_users'])) || $_REQUEST['user_id']  == $res['user_id']){
								
                                if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                                    $finalData[] = $data;
                                }else{
									$finalData[] = 	$data;
									//echo $data['distance'];
                                   /* if($user_data['pref_distance'] >= $data['distance'] ){
										if(!empty($res['age_max']) ){
											    if($diff >= $res['age'] && $diff <= $res['age_max']){
													$finalData[] = 	$data;
												}
												
											}else{
												if($diff >= $res['age']){
													$finalData[] = 	$data;
												}
											}
											
                                        
                                    }*/
									
                                }

                                // if($user_data['pref_distance'] >= $data['distance'] ){
                                //     $finalData[] = $data;
                                // }
                            }
                        }else{
                            if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                                $finalData[] = $data;
                            }else{
								
								if(!empty($user_data['pref_bash_type'])){
									if(in_array($data['bash_type'], explode(',',$user_data['pref_bash_type']))){

									}else{
										continue;
									}
								}
								
                                if($user_data['pref_distance'] >= $data['distance'] ){
									if(!empty($res['age_max'])){
										       if($diff >= $res['age'] && $diff <= $res['age_max']){
													$finalData[] = 	$data;
												}
												
											}else{
												if($diff >= $res['age']){
													$finalData[] = 	$data;
												}
											}
                                    
                                }
                            }
                            
                        }
                        
                    }else{
                        if($data['is_private'] == '1'){
                            if(in_array($_REQUEST['user_id'], explode(',', $res['name_of_host'])) || in_array($_REQUEST['user_id'], explode(',', $res['private_users'])) || $_REQUEST['user_id']  == $res['user_id']){
                                $finalData[] = $data;
                            }
                        }else{
                            $finalData[] = $data;
                        }
                    }
                }
            }
        }

        $finalData1 = array();
        if($_REQUEST['today'] == '1'){
            if(!empty($results1)){
                foreach($results1 as $res){ //print_r($res); die;
                    
                    $data = $this->get_bash_long($res['id'],$_REQUEST['user_id']);
                    if(!empty($data)){
                        //print_r($data); die;
                        if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                         
                        }else{
                            if(!empty($user_data['pref_bash_type'])){
                                if(in_array($data['bash_type'], explode(',',$user_data['pref_bash_type']))){
    
                                }else{
                                    continue;
                                }
                            }
                        }

                        if($user_data){
                            if($data['is_private'] == '1'){
                                if(in_array($_REQUEST['user_id'], explode(',', $res['name_of_host'])) || in_array($_REQUEST['user_id'], explode(',', $res['private_users'])) || $_REQUEST['user_id']  == $res['user_id']){
                                    if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                                        $finalData1[] = $data;
                                    }else{
                                        if($user_data['pref_distance'] >= $data['distance'] ){
                                            if(!empty($res['age_max'])){
												if($diff >= $res['age'] && $diff <= $res['age_max']){
													$finalData1[] = 	$data;
												}
												
											}else{
												if($diff >= $res['age']){
													$finalData1[] = 	$data;
												}
											}
                                        }
                                    }
                                }
                            }else{
                                if(in_array($_REQUEST['user_id'], explode(',',$data['name_of_host']))){
                                    $finalData1[] = $data;
                                }else{
                                    if($user_data['pref_distance'] >= $data['distance'] ){
                                        if($diff >= $data['age'] ){
										        $finalData1[] = $data;
									        }
                                    }
                                }
                               
                            }
                            
                        }else{
                            if($data['is_private'] == '1'){
                                if(in_array($_REQUEST['user_id'], explode(',', $res['name_of_host'])) || in_array($_REQUEST['user_id'], explode(',', $res['private_users'])) || $_REQUEST['user_id']  == $res['user_id']){
                                    $finalData1[] = $data;
                                }
                            }else{
                                $finalData1[] = $data;
                            }
                        }
                    }
                }
            }
        }
        if(!empty($finalData)){			
		    //print_r($finalData); die;
			/*$distance  = array_column($finalData, 'distance');
			$ratings = array_column($finalData, 'ratings');
			$complement = array_column($finalData, 'complement');
			$nofpeople = array_column($finalData, 'male')+array_column($finalData, 'fe_male')+array_column($finalData, 'not_specified');
             
			if(isset($_REQUEST['order']) && $_REQUEST['order']=='asc'){
				array_multisort($ratings, SORT_ASC, $complement, SORT_ASC, $nofpeople, SORT_ASC, $distance, SORT_ASC, $finalData); 
			}else{
				array_multisort($ratings, SORT_DESC, $complement, SORT_DESC, $nofpeople, SORT_DESC, $distance, SORT_DESC, $finalData); 
			}*/
			$start_date_time = array_column($finalData, 'start_date_time');
			array_multisort($start_date_time, SORT_ASC,$finalData); 
		}
		
		if(!empty($finalData1)){
			/*$distance  = array_column($finalData1, 'distance');
			$ratings = array_column($finalData1, 'ratings');
			$complement = array_column($finalData1, 'complement');
			$nofpeople = array_column($finalData1, 'male')+array_column($finalData1, 'fe_male')+array_column($finalData1, 'not_specified');

			if(isset($_REQUEST['order']) && $_REQUEST['order']=='asc'){
				array_multisort($ratings, SORT_ASC, $complement, SORT_ASC, $nofpeople, SORT_ASC, $distance, SORT_ASC, $finalData1); 
			}else{
				array_multisort($ratings, SORT_DESC, $complement, SORT_DESC, $nofpeople, SORT_DESC, $distance, SORT_DESC, $finalData1); 
			}  */
			
			$start_date_time = array_column($finalData1, 'start_date_time');
			array_multisort($start_date_time, SORT_ASC,$finalData1); 
		}
        if(!empty($_REQUEST['today'])){
			//$finalData = array_keys($finalData);
            $finalDatas = array('today'=> $finalData, 'upcomming'=> $finalData1);
        }else{
			//$finalData = array_keys($finalData);
            $finalDatas = array('today'=> $finalData, 'upcomming'=> $finalData1);
        }
        if(!empty($finalDatas)){
			
            api_response(200,'Bash listed successfully',$finalDatas);
        }else{
            api_response(200,'No data found','','array');
        }
    }

    public function near_by(){
        // $Redis = new RedisClient([
        //     'server' => '127.0.0.1:6379',
        //     'timeout' => 4
        // ]);

        if(!empty($_REQUEST['lat']) && !empty($_REQUEST['lng']) && isset($_REQUEST['user_id']) ){
			$user = User::where('id', '=', $_REQUEST['user_id'] )->first();
			
            $lat = $_REQUEST['lat'];
            $lon = $_REQUEST['lng'];
            $diff = 0;
			$user_data = $this->get_user($_REQUEST['user_id']);	
			if($user_data['dob'] == '0000-00-00'){
			}else{
			  $diff = (date('Y') - date('Y',strtotime($user_data['dob'])));
			}
            $datas = Bash::select("bashes.*"
                    ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                    * cos(radians(bashes.lat)) 
                    * cos(radians(bashes.lng) - radians(" . $lon . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(bashes.lat))) AS distance"))
                    //->where('user_id', '!=', $_REQUEST['user_id'] )
                    ->where('start_date','<=',date('Y-m-d'))
                    ->where('end_date_time','>=',date('Y-m-d H:i:s'))
					//->where('distance','=',$user->pref_distance)
                    ->orderBy('distance','asc')
                    ->get()->toArray();
                  //print_r($user); die;
                    $finalData = array();
                    if(!empty($datas)){
                        $i = 0;
                        foreach($datas as $key => $da){ 
                            $session_datas = $this->get_bash_long($da['id'],$_REQUEST['user_id']);
							
                            if(!empty($session_datas) && count($finalData) <= 20 && $da['distance'] <= $user->pref_distance){
                                if($da['is_private'] == '1'){
                                    if(in_array($_REQUEST['user_id'], explode(',', $da['private_users'])) ||$_REQUEST['user_id']  == $da['user_id']){
                                        $finalData[] = $session_datas;
                                    }
                                }else{
									if(!empty($da['age_max']) ){
										
										if($diff >= $da['age'] && $diff <= $da['age_max']){
											$finalData[] = 	$session_datas;
										}
										
									}else{
										if($diff >= $da['age']){
											$finalData[] = 	$session_datas;
										}
									}
                                    
                                }
                            }
                            $i++;
                        }
                    } 
           
            if(!empty($finalData)){
                api_response(200,'Bash listed successfully',$finalData);
            }else{
                api_response(200,'No BASH found. Increase search radius in BASH preferences for better results.','','array');
            }
        }
    }

    private function get_bash_long($bash_id,$other_id){
       
        try{

            $res = Bash::findorfail($bash_id)->toArray();
            // $Redis = new RedisClient([
            //     'server' => '127.0.0.1:6379',
            //     'timeout' => 4
            // ]);
            $lat = $res['lat'];
            $lng = $res['lng'];
            //echo date('Y-m-d H:i:s');
            if($res['start_date_time'] <= date('Y-m-d H:i:s') ){
                $res['delete']  = '0';
            }else{
                $res['delete']  = '1';
            }
            // $distance = $Redis->geopos("users.geo", $res['session_id']);
           
            // //print_r($distance); 
            // if(!empty($distance[0][0])){
            //     $lat = $distance[0][0];
            //     $res['lat'] = $lat;
            // }
            // if(!empty($distance[0][1])){
            //     $lng = $distance[0][1];
            //     $res['lng'] = $lng;
            // }
            
            if(!empty($lat) && !empty($lng) && !empty($_REQUEST['lat']) && !empty($_REQUEST['lng']) ){
                $res['distance'] = distance($_REQUEST['lat'], $_REQUEST['lng'],$res['lat'],$res['lng'],'m' );
            }else{
                $res['distance'] = '0.00' ;
            }
            // check deal crashed or not
            if(!empty($other_id)){
                //print_r($res['id']); die;
                $current_bashs = Crash::where('user_id', '=', $other_id )->where('bash_id', '=', $res['id'])->get()->toarray();
                if(!empty($current_bashs)){
                    $res['is_crash'] = 1 ;
                }else{
                    $res['is_crash'] = 0 ;
                }
                
                $current_bash_ticket = Ticket::where('user_id', '=', $other_id )->where('bash_id', '=', $res['id'])->get()->toarray();
                if(!empty($current_bash_ticket)){
                    $res['is_crash'] = 1 ;
                }

            }else{
                $res['is_crash'] = 0 ;
            }
            $res = $this->nullvalue_filter($res);
            //$finalData = array();
            $q1 = Ticket::query();
            $q1->where('bash_id','=',$bash_id);
            $q1->where('user_id','=',$other_id);
            $results123 = $q1->orderBy('id','DESC')->get()->toArray();
            if(!empty($results123[0])){
                
                // get bar code data
                $q2 = Qrcode::query();
                $q2->where('bash_id','=',$bash_id);
                $q2->where('user_id','=',$other_id);
                $resultss = $q2->orderBy('id','DESC')->get()->toArray();
                $barcodedata = array();
                if(!empty($resultss)){
                    foreach($resultss as $red){
                        //$red['used'] = '0';
                        $barcodedata[] = $red;
                    }
                }
                $results123[0]['barcodedata'] = $barcodedata;
                $res['ticket_data'] = $results123[0];
                //$res['used'] = '0';
                //$finalData[] = array('ticket'=> $results123[0], 'barcode'=> $barcodedata);
                
            }else{
                $res['ticket_data'] =  new \stdClass();
            }
            

            // get all hosts
            $hostArr = array();
            if(!empty($res['name_of_host'])){
                $name_of_hosts = explode(',', $res['name_of_host']);
                foreach($name_of_hosts as $name_of_host){
                    $user_data = $this->get_user($name_of_host);
                    if(!empty($user_data)){
                        $hostArr[] = array('id'=>$user_data['id'], 'fname'=>$user_data['fname'], 'lname'=>$user_data['lname'], 'username'=>$user_data['username'] );
                    }
                } 
            }
            $res['hosts'] = $hostArr;  
            $bash_rate_data = $this->get_bash_rate($res['user_id'],$bash_id);
            $bash_rate_data1 = $this->get_bash_rate1($bash_id);
            if($res['start_date'] == date('Y-m-d') ){
                if($res['start_time'] >= date('H:i:s') ){
                    $res['male'] = $bash_rate_data1['male'];
                    $res['fe_male'] = $bash_rate_data1['fe_male'];
                    $res['not_specified'] = $bash_rate_data1['not_specified'];
                    $res['ratings'] = $bash_rate_data['ratings'];
                    $res['complement'] = $bash_rate_data['complement'];
                    $res['complement1'] = $bash_rate_data['complement1'];
                    $res['complement2'] = $bash_rate_data['complement2'];
                    $res['complement3'] = $bash_rate_data['complement3'];
                    $res['complement4'] = $bash_rate_data['complement4'];
                    $res['complement5'] = $bash_rate_data['complement5'];
                    //$res = $this->nullvalue_filter($res);
                }else{
                    $res['male'] = $bash_rate_data1['male'];
                    $res['fe_male'] = $bash_rate_data1['fe_male'];
                    $res['not_specified'] = $bash_rate_data1['not_specified'];
                    $res['ratings'] = $bash_rate_data['ratings'];
                    $res['complement'] = $bash_rate_data['complement'];
                    $res['complement1'] = $bash_rate_data['complement1'];
                    $res['complement2'] = $bash_rate_data['complement2'];
                    $res['complement3'] = $bash_rate_data['complement3'];
                    $res['complement4'] = $bash_rate_data['complement4'];
                    $res['complement5'] = $bash_rate_data['complement5'];
                } 
            }else{
                $res['male'] = $bash_rate_data1['male'];
                $res['fe_male'] = $bash_rate_data1['fe_male'];
                $res['not_specified'] = $bash_rate_data1['not_specified'];
                $res['ratings'] = $bash_rate_data['ratings'];
                $res['complement'] = $bash_rate_data['complement'];
                $res['complement1'] = $bash_rate_data['complement1'];
                $res['complement2'] = $bash_rate_data['complement2'];
                $res['complement3'] = $bash_rate_data['complement3'];
                $res['complement4'] = $bash_rate_data['complement4'];
                $res['complement5'] = $bash_rate_data['complement5'];
                
            }
        return $res; 
        }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
            return array();
        }
    }
    private function get_bash_rate_user($user_id,$bash_ids){
        
        $ratings = '0';
        $rate_data =  Rating::whereIn('bash_id', $bash_ids )->where('skip', '=', '0' )->get()->toArray();
        $i = 0;
        $rat = 0;
        $complement = $complement1 = $complement2 = $complement3 = $complement4 = $complement5 = 0;
        if(!empty($rate_data)){
            foreach($rate_data as $rate_da){  //print_r($rate_da['complement']);
				$i++;
                $rat += $rate_da['rating'];
                if(!empty($rate_da['complement'])){
                    //echo "complement"; die;
                    $complement ++;
                }
                if(!empty($rate_da['complement']) && in_array('1',explode(',',$rate_da['complement']) ) ){
                    //print_r($rate_da); die;
                    $complement1++;
                }
                if(!empty($rate_da['complement']) && in_array('2',explode(',',$rate_da['complement']) ) ){
                    $complement2++;
                }
                if(!empty($rate_da['complement']) && in_array('3',explode(',',$rate_da['complement']) ) ){
                    $complement3++;
                }
                if(!empty($rate_da['complement']) && in_array('4',explode(',',$rate_da['complement']) ) ){
                    $complement4++;
                }
                if(!empty($rate_da['complement']) && in_array('5',explode(',',$rate_da['complement']) ) ){
                    $complement5++;
                }

            }
			

            $ratings = (string)round($rat/$i,'2');            
        }
        
        return array('ratings'=> $ratings , 'complement'=> $complement, 'complement1'=> $complement1, 'complement2'=> $complement2, 'complement3'=> $complement3, 'complement4'=> $complement4, 'complement5'=> $complement5);
    }
	
    private function get_bash_rate($bash_user_id,$bash_id=null){
        
        $male = 0;
        $fe_male = 0;
        $not_specified = 0;
        $complement = 0;
        // if(!empty($bash_id)){
        //     $checkin_data =  Checkin::where('bash_id', '=', $bash_id )->get()->toArray();
        // }else{
        //     $checkin_data =  Checkin::where('bash_user_id', '=', $bash_user_id )->get()->toArray();
        // }
        
        // $i = 1;
        // $rat = 0;
        // if(!empty($checkin_data)){
        //     foreach($checkin_data as $rate_da){
        //         $user_data = User::findorfail($rate_da['user_id']);
        //         //print_r($user_data); die;
        //         if($user_data->gender == '1'){
        //             $male ++;
        //         }else if($user_data->gender == '2'){
        //             $fe_male ++;
        //         }else{
        //             $not_specified ++;
        //         } 
        //     }
        // }

        $ratings = '3';
        $rate_data =  Rating::where('bash_user_id', '=', $bash_user_id )->where('skip', '=', '0' )->get()->toArray();
        $i = 0;
        $rat = 0;
        $complement1 = $complement2 = $complement3 = $complement4 = $complement5 = 0;
        if(!empty($rate_data)){
            foreach($rate_data as $rate_da){  //print_r($rate_da['complement']);
				$i++;
                $rat += $rate_da['rating'];
                if(!empty($rate_da['complement'])){
                    //echo "complement"; die;
                    $complement ++;
                }
                if(!empty($rate_da['complement']) && in_array('1',explode(',',$rate_da['complement']) ) ){
                    //print_r($rate_da); die;
                    $complement1++;
                }
                if(!empty($rate_da['complement']) && in_array('2',explode(',',$rate_da['complement']) ) ){
                    $complement2++;
                }
                if(!empty($rate_da['complement']) && in_array('3',explode(',',$rate_da['complement']) ) ){
                    $complement3++;
                }
                if(!empty($rate_da['complement']) && in_array('4',explode(',',$rate_da['complement']) ) ){
                    $complement4++;
                }
                if(!empty($rate_da['complement']) && in_array('5',explode(',',$rate_da['complement']) ) ){
                    $complement5++;
                }

            }
			

            $ratings = (string)round($rat/$i,'2');            
        }
        
        return array('male'=> (string)$male, 'fe_male'=> (string)$fe_male, 'not_specified'=> (string)$not_specified, 'ratings'=> $ratings , 'complement'=> $complement, 'complement1'=> $complement1, 'complement2'=> $complement2, 'complement3'=> $complement3, 'complement4'=> $complement4, 'complement5'=> $complement5);
    }
    
    private function get_bash_age($bash_id=null){
        
        
		$agegroup = array(
		   array('age'=> '-18', 'male'=>0, 'fe_male'=>0, 'not_specified'=>0),
		   array('age'=> '18-20', 'male'=>0, 'fe_male'=>0, 'not_specified'=>0),
		   array('age'=> '21-30', 'male'=>0, 'fe_male'=>0, 'not_specified'=>0),
		   array('age'=> '31-40', 'male'=>0, 'fe_male'=>0, 'not_specified'=>0),
		   array('age'=> '41-50', 'male'=>0, 'fe_male'=>0, 'not_specified'=>0),
		   array('age'=> '50+', 'male'=>0, 'fe_male'=>0, 'not_specified'=>0)
		);
		
        $bash_data_rate1 =  Bash::where('id', '=', $bash_id )->get()->toArray();
        
      
        $checkin_data =  Checkin::where('bash_id', '=', $bash_id )->where('skip', '=', '0' )->get()->toArray();
		//print_r($checkin_data); die;
        $avg_time = "0.00";
		$person = 0;
        if(!empty($checkin_data)){
            foreach($checkin_data as $rate_da){
                $user_data = User::findorfail($rate_da['user_id']);
				
				$loc_datas =  LocationUpdate::where('bash_id', '=', $bash_id )->where('user_id', '=', $rate_da['user_id'] )->get()->toArray();
				$person +=  LocationUpdate::where('bash_id','<=', $bash_id )->where('user_id', '=', $rate_da['user_id'] )->distinct()->get(['user_id'])->count();
				
				//print_r($loc_datas); die;
				if(!empty($loc_datas)){
					//echo $loc_datas[0]['created_at'];
					//echo $loc_datas[count($loc_datas)-1]['created_at'];
					$start_time = strtotime($loc_datas[0]['start_time']);
					$end_time = strtotime($loc_datas[0]['end_time']);
					$avg_time += round(abs($start_time - $end_time) / 60,2);
				}
			    //print_r($avg_time); die;
			
                $diff = 0;
				if($user_data->dob == '0000-00-00'){
				}else{
				  $diff = (date('Y') - date('Y',strtotime($user_data->dob)));
				}
				if($diff <= 18){
				    if($user_data->gender == '1'){
						//$male ++;
						$agegroup[0]['male'] = $agegroup[0]['male']+1; 
					}else if($user_data->gender == '2'){
						$agegroup[0]['fe_male'] = $agegroup[0]['fe_male']+1; 
					}else{
						$agegroup[0]['not_specified'] = $agegroup[0]['not_specified']+1; 
					} 
               }
				if($diff >= 18 && $diff <= 20){
				    if($user_data->gender == '1'){
						//$male ++;
						$agegroup[1]['male'] = $agegroup[1]['male']+1; 
					}else if($user_data->gender == '2'){
						$agegroup[1]['fe_male'] = $agegroup[1]['fe_male']+1; 
					}else{
						$agegroup[1]['not_specified'] = $agegroup[1]['not_specified']+1; 
					} 
               }
			   if($diff >= 21 && $diff <= 30){
				    if($user_data->gender == '1'){
						//$male ++;
						$agegroup[2]['male'] = $agegroup[2]['male']+1; 
					}else if($user_data->gender == '2'){
						$agegroup[2]['fe_male'] = $agegroup[2]['fe_male']+1; 
					}else{
						$agegroup[2]['not_specified'] = $agegroup[2]['not_specified']+1; 
					} 
               }
			   if($diff >= 31 && $diff <= 40){
				    if($user_data->gender == '1'){
						//$male ++;
						$agegroup[3]['male'] = $agegroup[3]['male']+1; 
					}else if($user_data->gender == '2'){
						$agegroup[3]['fe_male'] = $agegroup[3]['fe_male']+1; 
					}else{
						$agegroup[3]['not_specified'] = $agegroup[3]['not_specified']+1; 
					} 
               }
			   if($diff >= 41 && $diff <= 50){
				    if($user_data->gender == '1'){
						//$male ++;
						$agegroup[4]['male'] = $agegroup[4]['male']+1; 
					}else if($user_data->gender == '2'){
						$agegroup[4]['fe_male'] = $agegroup[4]['fe_male']+1; 
					}else{
						$agegroup[4]['not_specified'] = $agegroup[4]['not_specified']+1; 
					} 
               }
			   if($diff >= 50){
				    if($user_data->gender == '1'){
						//$male ++;
						$agegroup[5]['male'] = $agegroup[5]['male']+1; 
					}else if($user_data->gender == '2'){
						$agegroup[5]['fe_male'] = $agegroup[5]['fe_male']+1; 
					}else{
						$agegroup[5]['not_specified'] = $agegroup[5]['not_specified']+1; 
					} 
               }
			   
            }
        }
        
		//print_r($agegroup); die;
        if(!empty($bash_data_rate1[0]['name_of_host'])){
            $hostdatas = explode(',', $bash_data_rate1[0]['name_of_host']);
            
            foreach($hostdatas as $hostd){
                try {
                    $user_data = User::findorfail($hostd);
					
					$loc_datas =  LocationUpdate::where('bash_id', '=', $bash_id )->where('user_id', '=', $hostd )->get()->toArray();
					//$person +=  LocationUpdate::where('bash_id','<=', $bash_id )->where('user_id', '=', $hostd )->distinct()->get(['user_id'])->count();
					if(!empty($loc_datas)){
						$person++;
						$start_time = strtotime($loc_datas[0]['start_time']);
						$end_time = strtotime($loc_datas[0]['end_time']);
						$avg_time += round(abs($start_time - $end_time) / 60,2);
					}
			        
				
                    $diff = 0;
					if($user_data->dob == '0000-00-00'){
					}else{
					  $diff = (date('Y') - date('Y',strtotime($user_data->dob)));
					}
					if($diff <= 18){
						if($user_data->gender == '1'){
							//$male ++;
							$agegroup[0]['male'] = $agegroup[0]['male']+1; 
						}else if($user_data->gender == '2'){
							$agegroup[0]['fe_male'] = $agegroup[0]['fe_male']+1; 
						}else{
							$agegroup[0]['not_specified'] = $agegroup[0]['not_specified']+1; 
						} 
				   }
					if($diff >= 18 && $diff <= 20){
						if($user_data->gender == '1'){
							//$male ++;
							$agegroup[1]['male'] = $agegroup[1]['male']+1; 
						}else if($user_data->gender == '2'){
							$agegroup[1]['fe_male'] = $agegroup[1]['fe_male']+1; 
						}else{
							$agegroup[1]['not_specified'] = $agegroup[1]['not_specified']+1; 
						} 
				   }
				   if($diff >= 21 && $diff <= 30){
						if($user_data->gender == '1'){
							//$male ++;
							$agegroup[2]['male'] = $agegroup[2]['male']+1; 
						}else if($user_data->gender == '2'){
							$agegroup[2]['fe_male'] = $agegroup[2]['fe_male']+1; 
						}else{
							$agegroup[2]['not_specified'] = $agegroup[2]['not_specified']+1; 
						} 
				   }
				   if($diff >= 31 && $diff <= 40){
						if($user_data->gender == '1'){
							//$male ++;
							$agegroup[3]['male'] = $agegroup[3]['male']+1; 
						}else if($user_data->gender == '2'){
							$agegroup[3]['fe_male'] = $agegroup[3]['fe_male']+1; 
						}else{
							$agegroup[3]['not_specified'] = $agegroup[3]['not_specified']+1; 
						} 
				   }
				   if($diff >= 41 && $diff <= 50){
						if($user_data->gender == '1'){
							//$male ++;
							$agegroup[4]['male'] = $agegroup[4]['male']+1; 
						}else if($user_data->gender == '2'){
							$agegroup[4]['fe_male'] = $agegroup[4]['fe_male']+1; 
						}else{
							$agegroup[4]['not_specified'] = $agegroup[4]['not_specified']+1; 
						} 
				   }
				   if($diff >= 50){
						if($user_data->gender == '1'){
							//$male ++;
							$agegroup[5]['male'] = $agegroup[5]['male']+1; 
						}else if($user_data->gender == '2'){
							$agegroup[5]['fe_male'] = $agegroup[5]['fe_male']+1; 
						}else{
							$agegroup[5]['not_specified'] = $agegroup[5]['not_specified']+1; 
						} 
				   }
                }catch (\Illuminate\Database\QueryException $exception){
                
                 }catch(\Exception $exception){
                 //api_response(404,$exception->getMessage());
                }
				
                
               
            }
        }
        //print_r($avg_time); 
				//echo "person";
              //  print_r($person); die;
        return array('agegroup'=> $agegroup,'avg_time'=> !empty($person)?number_format(($avg_time/$person),'2'):'0.00');
    }
	
    private function get_bash_rate1($bash_id=null){
        
        $male = 0;
        $fe_male = 0;
        $not_specified = 0;
        $complement = 0;
        $bash_data_rate1 =  Bash::where('id', '=', $bash_id )->get()->toArray();
        
      
        $checkin_data =  Checkin::where('bash_id', '=', $bash_id )->where('skip', '=', '0' )->get()->toArray();
       
        if(!empty($checkin_data)){
            foreach($checkin_data as $rate_da){
                $user_data = User::findorfail($rate_da['user_id']);
                //print_r($user_data); die;
                if($user_data->gender == '1'){
                    $male ++;
                }else if($user_data->gender == '2'){
                    $fe_male ++;
                }else{
                    $not_specified ++;
                } 
            }
        }
        // if($bash_id == '175'){
        //     print_r($bash_data_rate1); die;
        // }
        
        if(!empty($bash_data_rate1[0]['name_of_host'])){
            $hostdatas = explode(',', $bash_data_rate1[0]['name_of_host']);
            
            foreach($hostdatas as $hostd){
                try {
                    $user_data = User::findorfail($hostd);
                    if($user_data->gender == '1'){
                        $male ++;
                    }else if($user_data->gender == '2'){
                        $fe_male ++;
                    }else{
                        $not_specified ++;
                    } 
                }catch (\Illuminate\Database\QueryException $exception){
                
                 }catch(\Exception $exception){
                 //api_response(404,$exception->getMessage());
                }
                
                //print_r($user_data); die;
               
            }
        }

        return array('male'=> (string)$male, 'fe_male'=> (string)$fe_male, 'not_specified'=> (string)$not_specified );
    }

    public function crash_the_bash(Request $request){
        
        if(!empty($_REQUEST['user_id'])  && !empty($_REQUEST['bash_id']) ){
            try {
               
                $new_count =  Crash::where('user_id', '=', $request->user_id )->where('bash_id', '=', $request->bash_id)->count();
                if($new_count <= 0){
                    $crash = new Crash();
                    $crash->user_id = $request->user_id;
                    $crash->bash_id = $request->bash_id;
                    $crash->save();
                    api_response(200,'Event details have been added to My BASH',array('count'=>$this->get_crash_count_today($request->user_id)));
                }else{
                    api_response(200,'Bash crashed already',array('count'=>$this->get_crash_count_today($request->user_id)));
                }
                
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function add_to_bash(Request $request){
        if(!empty($_REQUEST['user_id'])  && !empty($_REQUEST['bash_id']) ){
            try{
                $bash = Bash::findorfail($_REQUEST['bash_id']);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Please enter valid bash id');
            }
            if(!empty($bash['private_users'])){
                $bash1 = $bash->toarray();
                $old_data = explode(',',$bash1['private_users']);
                $new_users = explode(',',$_REQUEST['user_id']);
                $unique_users = array_diff($new_users,$old_data);
                //print_r($unique_users);
                $user = $this->get_user($bash['user_id']);
                $mes = $user['fname']." ".$user['lname']." send you an event invitation"; 

                foreach($unique_users as $unique_user){
                    $this->save_notify($unique_user,'New invitation',$mes,'invitation',$_REQUEST['bash_id']);
                }
                // notify new users only

                $final_data = array_merge($old_data, $new_users);
                $final_data = array_unique($final_data);
                $bash->private_users = implode(',',$final_data);
            }else{
                $user = $this->get_user($bash['user_id']);
                $mes = $user['fname']." ".$user['lname']." sent you an event invitation to ".$bash['name']; 
                $bash->private_users = $_REQUEST['user_id'];
                $unique_users = explode(',',$_REQUEST['user_id']);
                foreach($unique_users as $unique_user){
                    $this->save_notify($unique_user,'New invitation',$mes,'invitation',$_REQUEST['bash_id']);
                }
            }
            $bash->save();
            api_response(200,'Invitation has been sent successfully.');
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function get_crash_list(Request $request){
		 $result = "request of crash list "; 
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true));
		
        if(!empty($_REQUEST['user_id'])){
            try{
                $user_id = $_REQUEST['user_id'];
                $current_bashh = $current_bash_arr = $current_bash_id_arr = array();
                

                $currenthosts =  Bash::where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'))->whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->get()->toarray();
                if(!empty($currenthosts)){
                    foreach($currenthosts as $curren){
                        $current_bash_id_arr[] = $curren['id'];
                    }
                }
                
                $current_bashs_ticket = Ticket::where('user_id', '=', $_REQUEST['user_id'] )->whereHas('bash', function ($query) {
                    $query->where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'));
                })->get()->toarray();
                
                if(!empty($current_bashs_ticket)){
                    foreach($current_bashs_ticket as $current_bashs_ti){ //print_r($current_bash); die;
                        $current_bash_id_arr[] = $current_bashs_ti['bash_id'];
                    }
                }

                $current_bashs = Crash::where('user_id', '=', $_REQUEST['user_id'] )->whereHas('bash', function ($query) {
                    $query->where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'));
                })->get()->toarray();
                
                if(!empty($current_bashs)){
                    foreach($current_bashs as $current_bash){ //print_r($current_bash); die;
                        $current_bash_id_arr[] = $current_bash['bash_id'];
                    }
                }

                //print_r($currenthosts); die;
                // 1) hosts
                // tickets buy
                // crash bash

                if(!empty($current_bash_id_arr)){
                    foreach($current_bash_id_arr as $current_bash_id){ //print_r($current_bash); die;
                        $current_bashh['bash_data'] = $this->get_bash_long($current_bash_id,$_REQUEST['user_id']);
                        $current_bashh['user_data'] = $this->get_user($_REQUEST['user_id']);
                        $current_bash_arr[] = $current_bashh;
                    } 
                }

                
                $upcomming_bash_arr = $upcomming_bash_id_arr = array();
                $upcomming_bashs = Crash::where('user_id', '=', $_REQUEST['user_id'] )->whereHas('bash', function ($query) {
                    $query->where('start_date', '>', date('Y-m-d') );
                })->get()->toarray();


                if(!empty($upcomming_bashs)){
                    foreach($upcomming_bashs as $upcomming_bash){ //print_r($current_bash); die;
                        $upcomming_bash_id_arr[] = $upcomming_bash['bash_id'];
                    }
                }

                $upcomminghosts =  Bash::where('start_date','>', date('Y-m-d'))->whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->get()->toarray();
                if(!empty($upcomminghosts)){
                    foreach($upcomminghosts as $upcomm){
                        $upcomming_bash_id_arr[] = $upcomm['id'];
                    }
                }

                $upcomming_bashs_ticket = Ticket::where('user_id', '=', $_REQUEST['user_id'] )->whereHas('bash', function ($query) {
                    $query->where('start_date','>', date('Y-m-d'));
                })->get()->toarray();
                
                if(!empty($upcomming_bashs_ticket)){
                    foreach($upcomming_bashs_ticket as $upcomming_bashs_ti){ //print_r($current_bash); die;
                        $upcomming_bash_id_arr[] = $upcomming_bashs_ti['bash_id'];
                    }
                }



                if(!empty($upcomming_bash_id_arr)){
                    foreach($upcomming_bash_id_arr as $upcomming_bash_id){
                        $upcomming_bash['bash_data'] = $this->get_bash_long($upcomming_bash_id,$_REQUEST['user_id']);
                        $upcomming_bash['user_data'] = $this->get_user($_REQUEST['user_id']);
                        $upcomming_bash_arr[] = $upcomming_bash;
                    }
                }
				 $result = "response  of crash list ";
		         //$result .= json_encode(array('today'=> $current_bash_arr, 'upcomming'=> $upcomming_bash_arr ));
		        //\Log::info(print_r($result, true));
				$current_bash_arr = Arr::sort($current_bash_arr, function($data)
				{
					return $data['bash_data']['start_date_time'];
				});
				$upcomming_bash_arr = Arr::sort($upcomming_bash_arr, function($data)
				{
					return $data['bash_data']['start_date_time'];
				});
		         //$start_date_time = array_column($current_bash_arr, 'start_date_time');
				// array_multisort($start_date_time, SORT_ASC, $current_bash_arr);
				 // $start_date_time1 = array_column($upcomming_bash_arr, 'start_date_time');
				// array_multisort($start_date_time1, SORT_ASC, $upcomming_bash_arr);
				$current_bash_arr = array_values($current_bash_arr);
				$upcomming_bash_arr = array_values($upcomming_bash_arr);
                api_response(200,'',array('today'=> $current_bash_arr, 'upcomming'=> $upcomming_bash_arr ));
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function ping(Request $request){
        //echo date('Y-m-d H:i:s'); die;
		$result = "request of ping ";
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true));
		date_default_timezone_set('UTC');
        if(!empty($_REQUEST['session_id'])){
            try{
                $user = User::where('session_id', '=', $request->session_id )->first();
            
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Please enter valid user id');
            } 
            if(!$user){
                api_response(404,'Please enter valid user id');
            }
          
          
          $user->lat = $request->lat;
          $user->lng = $request->long;
          $user->last_location = date('Y-m-d H:i:s');
		  if(!empty($_REQUEST['timezone'])){
			  $user->timezone = $_REQUEST['timezone'];
		  }
		  $user->save();
		  
         if(!empty($_REQUEST['timezone'])){
			 date_default_timezone_set($_REQUEST['timezone']);
		 } 
          // notify if user has free bash
          // get crash event and today:
         
        $current_bashs = Crash::where('user_id', '=', $user->id)->whereHas('bash', function ($query) {
            $query->where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'));
        })->get()->toarray();
        //print_r($current_bashs); die;
        if(!empty($current_bashs)){
            $lat = $request->lat;
            $lon = $request->long;
            foreach($current_bashs as $current_bash){
                $datas_bashes = Bash::select("bashes.id","bashes.name","bashes.free_notify"
                ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                * cos(radians(bashes.lat)) 
                * cos(radians(bashes.lng) - radians(" . $lon . ")) 
                + sin(radians(" .$lat. ")) 
                * sin(radians(bashes.lat))) AS distance"))
                //->where('distance', '<=', '500' )
                ->where('id','=',$current_bash['bash_id'])
                //->where('end_date_time','>=',date('Y-m-d H:i:s'))
                ->orderBy('distance','asc')
                ->get()->toArray();
                if(!empty($datas_bashes)){
                    foreach($datas_bashes as $datas_bashe){
                        //print_r(explode(',', $datas_bashe['free_notify']));
                        if(!in_array($user->id, explode(',', $datas_bashe['free_notify']))){
                            if($datas_bashe['distance'] <= 0.310686){ //print_r($datas_bashe); die;
                                $this->notify_free($current_bash['bash_id'], $user->id , 'add');
                                $mes = "Check into this event, '".$datas_bashe['name']. "' ?";
                                $this->save_notify($user->id,'Event Check-in', $mes,'near_event');
                            }
                        } 
                    }
                }
                //print_r($datas); die;
            }
            
        }

        // update check in user location
        $checkin_bashs = Checkin::where('user_id', '=', $user->id)->where('skip', '=', '0')->whereHas('bash', function ($query) {
            $query->where('start_date_time','<=', date('Y-m-d H:i:s'))->where('end_date_time','>=', date('Y-m-d H:i:s'));
        })->get()->toarray();
 		
		if(!empty($checkin_bashs)){
            $lat = $request->lat;
            $lon = $request->long;
            foreach($checkin_bashs as $current_bash){
                $datas_bashes = Bash::select("bashes.id","bashes.name","bashes.free_notify"
                ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                * cos(radians(bashes.lat)) 
                * cos(radians(bashes.lng) - radians(" . $lon . ")) 
                + sin(radians(" .$lat. ")) 
                * sin(radians(bashes.lat))) AS distance"))
                //->where('distance', '<=', '500' )
                ->where('id','=',$current_bash['bash_id'])
                //->where('end_date_time','>=',date('Y-m-d H:i:s'))
                ->orderBy('distance','asc')
                ->get()->toArray();
				
                if(!empty($datas_bashes)){
                    foreach($datas_bashes as $datas_bashe){
                        if($datas_bashe['distance'] <= 0.310686){ //print_r($datas_bashe); die;
						    $loc_find =  LocationUpdate::where('user_id','=',$user->id)->where('bash_id','=',$datas_bashe['id'])->orderBy('id','desc')->first();
							
							
							//echo date('Y-m-d H:i:s',strtotime("+45 minutes"));
							if(!empty($loc_find)){
								$loc_find->end_time = date('Y-m-d H:i:s');
								$loc_find->save();
							}else{
								$location = new LocationUpdate();
								$location->user_id = $user->id;
								$location->bash_id = $datas_bashe['id'];
								$location->start_time = date('Y-m-d H:i:s');
								$location->end_time = date('Y-m-d H:i:s');
								$location->save();
							}
						}
                    }
                }
                //print_r($datas); die;
            }
            
        }
		
		// update check in user location
       
        $host_users = Bash::where('start_date_time','<=', date('Y-m-d H:i:s'))->where('end_date_time','>=', date('Y-m-d H:i:s'))->whereRaw('FIND_IN_SET('.$user->id.',name_of_host)')->get()->toarray();
 		
		if(!empty($host_users)){
            $lat = $request->lat;
            $lon = $request->long;
            foreach($host_users as $current_bash){
                $datas_bashes = Bash::select("bashes.id","bashes.name","bashes.free_notify"
                ,DB::raw("3959 * acos(cos(radians(" . $lat . ")) 
                * cos(radians(bashes.lat)) 
                * cos(radians(bashes.lng) - radians(" . $lon . ")) 
                + sin(radians(" .$lat. ")) 
                * sin(radians(bashes.lat))) AS distance"))
                //->where('distance', '<=', '500' )
                ->where('id','=',$current_bash['id'])
                //->where('end_date_time','>=',date('Y-m-d H:i:s'))
                ->orderBy('distance','asc')
                ->get()->toArray();
				
                if(!empty($datas_bashes)){
                    foreach($datas_bashes as $datas_bashe){ //print_r($datas_bashe); die;
                        if($datas_bashe['distance'] <= 0.310686){ //print_r($datas_bashe); die;
						    $loc_find =  LocationUpdate::where('user_id','=',$user->id)->where('bash_id','=',$datas_bashe['id'])->orderBy('id','desc')->first();
							
							
							//echo date('Y-m-d H:i:s',strtotime("+45 minutes"));
							if(!empty($loc_find)){
								$loc_find->end_time = date('Y-m-d H:i:s');
								$loc_find->save();
							}else{
								$location = new LocationUpdate();
								$location->user_id = $user->id;
								$location->bash_id = $datas_bashe['id'];
								$location->start_time = date('Y-m-d H:i:s');
								$location->end_time = date('Y-m-d H:i:s');
								$location->save();
							}
						}
                    }
                }
                //print_r($datas); die;
            }
            
        }
		
        $result = "response of ping ";
		//$result .= json_encode($this->get_user($user->id));
		//\Log::info(print_r($result, true)); 
		
          //print_r($user->id); die;
            api_response(200,'Data updated successfully');
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function cron1(){
		
        //echo date("d-m-Y H:i:s"); die;
        $past_bashes =  Bash::where('rating_notify','=','0')->get()->toarray();
        //print_r($past_bashes); die;
        if(!empty($past_bashes)){
            foreach($past_bashes as $past_bash){  //print_r($past_bash);
                if(!empty($past_bash['timezone'])){
                    date_default_timezone_set($past_bash['timezone']);
                }
                if($past_bash['end_date_time'] < date('Y-m-d H:i:s',strtotime("-12 hours.") )){

                    $checkin_datas =  Checkin::where('skip', '=', '0')->where('bash_id', '=', $past_bash['id'])->get()->toArray();
                     // whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')    
                  

                    //print_r($checkin_datas); die;
                    if(!empty($checkin_datas)){
                        foreach($checkin_datas as $checki){ 
                            // check bash host
                            //$bash_data = Bash::where('id', '=', $checki['bash_id'])->first();
                            //print_r($checki);
                           
                                $user = User::findorfail($checki['user_id']);
                                if(!empty($user->pending_rating)){
                                    $notify_data = explode(',', $user->pending_rating);
                                    if(!in_array($past_bash['id'], $notify_data)){
                                        $notify_data[] = $past_bash['id'];
                                        $user->pending_rating = implode(',',$notify_data);
                                    }
                                }else{
                                    $user->pending_rating = $past_bash['id'];
                                }
                                $mes = $past_bash['name']." event has been completed. Click here to give rating to this event.";
                                $this->save_notify($checki['user_id'],'Give rating', $mes,'cron_notify_rating');
                                $user->save();  
                            }
                            
                        //}
                    }
                   // die;

                    /* if(!empty($past_bash['name_of_host'])){
                        $hostdatas = explode(',', $past_bash['name_of_host']);
                        
                        foreach($hostdatas as $hostd){
                            if($past_bash['user_id'] != $hostd){
                                $user = User::findorfail($hostd);
                            
                                if(!empty($user->pending_rating)){
                                    $notify_data = explode(',', $user->pending_rating);
                                    if(!in_array($past_bash['id'], $notify_data)){
                                        $notify_data[] = $past_bash['id'];
                                        $user->pending_rating = implode(',',$notify_data);
                                    }
                                }else{
                                    $user->pending_rating = $past_bash['id'];
                                }
                                $mes = $past_bash['name']." event has been completed. Click here to give rating to this event.";
                                $this->save_notify($hostd,'Give rating', $mes,'cron_notify_rating');
                                $user->save(); 
                            }
                        }
                    }  */
 
                    $bash = Bash::findorfail($past_bash['id']);
                    $bash->rating_notify = '1';
                    $bash->save(); 
               }
            }
        }
        //print_r($past_bashes); die;
        die;
    }
    
    public function cron2(){
        //not users after 3 days and make temp wallet to wallet

       // $pending_wallets = TempWallet::whereHas('bash', function ($query) {
         //   $query->where('end_date_time','<=',  date('Y-m-d H:i:s',strtotime("-3 days.") ) );
        //})->get()->toarray();
        
        $pending_wallets =  TempWallet::get()->toarray();
       // print_r($pending_wallet); die;
        if(!empty($pending_wallets)){
            $notify_data = array();
            foreach($pending_wallets as $pending_wallet){  //print_r($pending_wallet->bash);
                $bash_data = Bash::where('id', '=', $pending_wallet['bash_id'])->first();
                if(!empty($bash_data)){
                    if(!empty($bash_data['timezone'])){
                        date_default_timezone_set($bash_data['timezone']);
                    }
                    
                    if($bash_data['end_date_time'] <= date('Y-m-d H:i:s',strtotime("-3 days")) ){
                        
                        
                        if(in_array($pending_wallet['user_id'], array_column($notify_data, 'user_id'))) { 
                           
                            $amount = $pending_wallet['amount']+ $notify_data[$pending_wallet['user_id']]['amt'];
                            $mes = "$".$amount. " has been added to wallet. Now you can redeem it.";
                            $notify_data[$pending_wallet['user_id']] = array('user_id'=> $pending_wallet['user_id'], 'mes'=> $mes,'amt'=> $amount);
                        }else{
                            $amount += $pending_wallet['amount'];
                            $mes = "$".$amount. " has been added to wallet. Now you can redeem it.";
                            $notify_data[$pending_wallet['user_id']] = array('user_id'=> $pending_wallet['user_id'], 'mes'=> $mes, 'amt'=> $amount);
                        }

                       

                        $this->add_wallet($pending_wallet['user_id'],$pending_wallet['amount'],'credit',$pending_wallet['bash_id']);
                        TempWallet::where('id','=',$pending_wallet['id'])->delete();
                    }
                } 
            }
            if(!empty($notify_data)){
                foreach($notify_data as $notify_da){
                    $this->save_notify($notify_da['user_id'],'Wallet added', $notify_da['mes'],'walleta_added');
                }
            }
           
        }    
               
        //print_r($past_bashes); die;
        die;
    }
    
    public function cron3(){
		//echo "hi"; die;
        require "public/paypal_payout/vendor/autoload.php";
		
    //     $curl = curl_init("https://api.sandbox.paypal.com/v1/checkout/orders/8RJ002357P676940Y");
    //     curl_setopt($curl, CURLOPT_POST, false);
    //     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($curl, CURLOPT_HEADER, false);
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    //         'Authorization: Bearer AeZJFulZsHdO-HCJFRrDscpZ8Or7cN8PakwuTNUSdvi9dVo1h6KCOQD_vUftVE2kUXD13GgVvUQTNGkl:EHtAirUF1n3E6zGVOiDXb6rCls04rlOZt3kjrKg-p2thr8q2U2poBfN2e0aK6yM4IIP68HmvaXUgdJ4O',
    //         'Accept: application/json',
    //         'Content-Type: application/json'
    //     ));
    //     $response = curl_exec($curl);
    //     $result = json_decode($response);

    // print_r($result); die;
    
        $pending_wallets =  Wallet::where('payout_item_id',  '!=', '')->where('transaction_status', '!=', 'SUCCESS')->where('setteled', '==', '0')->get()->toarray();
        //print_r($pending_wallets); 
        if(!empty($pending_wallets)){
            /* $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    'AeZJFulZsHdO-HCJFRrDscpZ8Or7cN8PakwuTNUSdvi9dVo1h6KCOQD_vUftVE2kUXD13GgVvUQTNGkl',     // ClientID
                    'EHtAirUF1n3E6zGVOiDXb6rCls04rlOZt3kjrKg-p2thr8q2U2poBfN2e0aK6yM4IIP68HmvaXUgdJ4O'      // ClientSecret
                )
            ); */
            
			$apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    'AajbMuzyIRMlfOqWgztSvi8WgtBwwBTYI4aWA4C06CcdJOV3FRE5qNN_9h6zAL0UTObo8Lm9S2JFAWPS',     // ClientID
                    'EE_6kmbI-WegHEkMF_fdxHwIhn7BZgnfzZjtXb8e0KoUNm5zzZ3UUDA3XeBtAcSVWVNS8oQYw3H3adZs'      // ClientSecret
                )
            );
            $apiContext->setConfig(
				  array(
					'mode' => 'live',
					)
			);
			
            $notify_data = array();
            foreach($pending_wallets as $pending_wallet){  //print_r($pending_wallet); 
                $payoutBatchId = $pending_wallet['payout_item_id'];
                $payoutBatch = \PayPal\Api\Payout::get($payoutBatchId, $apiContext);
                $payoutItems = $payoutBatch->getItems();
                //print_r($payoutItems[0]);
                $status = $payoutItems[0]->transaction_status;
                $wallet = Wallet::where('id', '=', $pending_wallet['id'])->first();
                $wallet->transaction_status = $status;
                $wallet->transcation_id = $payoutItems[0]->transaction_id;
                
				if($pending_wallet['transaction_status'] == 'FAILED' || $pending_wallet['transaction_status'] == 'RETURNED'){
					$this->add_wallet($pending_wallet['user_id'],$pending_wallet['amount'],'credit','');
					$wallet->setteled = '1';
					$amount = $pending_wallet['amount'];
					$mess =  "$".$amount. " has been returned to your bash balance. Please check your PayPal information and try again.";
					$this->save_notify($pending_wallet['user_id'],'Wallet added', $mess,'walleta_added');
				}
				$wallet->save();
			}
        }    
               
        //print_r($past_bashes); die;
        die;
    }
    
	
	
	 
	
	public function cron5(){
		//echo  date('Y-m-d H:i:s');
			$current_bashs = Crash::where('notify','=','0')->whereHas('bash', function ($query) {
                    //$query->where('start_date_time','>=', date('Y-m-d H:i:s'))->where('start_date_time','<=', date('Y-m-d H:i:s',strtotime('+3 hours')));
                })->get()->toarray();
            //print_r($current_bashs); die;    
                if(!empty($current_bashs)){
                    foreach($current_bashs as $current_bash){ 
                        $bash_data = Bash::where('id', '=', $current_bash['bash_id'])->first();
                        if(!empty($bash_data->timezone)){
                            date_default_timezone_set($bash_data->timezone);
                        }
						//echo  date('Y-m-d H:i:s');
						//echo date('Y-m-d H:i:s',strtotime('+3 hours'));  
						//die;
                        if($bash_data->start_date_time >= date('Y-m-d H:i:s',strtotime('+170 minutes'))  &&  $bash_data->start_date_time <= date('Y-m-d H:i:s',strtotime('+3 hours')) ){
							$mess = $bash_data->name. " starts in 3 hours";
							$this->save_notify($current_bash['user_id'],'Upcoming event', $mess,'upcoming_event');
							$crash = Crash::where('id', '=', $current_bash['id'])->first();
							$crash->notify = '1';
							$crash->save();
						}						
                    }
                }
				
			
				   
			//print_r($past_bashes); die;
			die;
	}
	
    private function notify_free($bash_id,$user_id,$action){
        $bash = Bash::findorfail($bash_id);
        //print_r($bash);
        if($action == 'add'){
          if(!empty($bash->free_notify)){
            $notify_data = explode(',', $bash->free_notify);
            if(!in_array($user_id, $notify_data)){
                $notify_data[] = $user_id;
                $bash->free_notify = implode(',',$notify_data);
            }
            
          }else{
            $bash->free_notify = $user_id;
          }
        }else{
            $notify_data = explode(',' ,$bash->free_notify);
            if (($key = array_search($user_id, $notify_data)) !== false) {
                unset($notify_data[$key]);
            }
            $bash->free_notify = implode(',',$notify_data);
        }
        //print_r($bash->free_notify); die;
        $bash->save();
    }

    public function get_user_location_by_session_id(Request $request){
		$result = "request of get_user_location_by_session_id ";
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true)); 
		
        if(!empty($_REQUEST['session_ids'])){
            try{
                $final_data = array();
                $all_sessions = explode(',',$_REQUEST['session_ids']);
                //print_r($all_sessions);
                $all_users = User::select('session_id','lat','lng','last_location')->whereIn('session_id', $all_sessions)->get()->toArray();
                foreach($all_users as $all_user){ 
                    $final_data[] = array('session_id'=> $all_user['session_id'], 'lat'=> !empty($all_user['lat'])?$all_user['lat']:'0', 'lng'=> ($all_user['lng'])?$all_user['lng']:'0', 'last_location'=> !empty($all_user['lat'])?time_elapsed_string($all_user['last_location']):'Not updated yet' );
                }
                
                $result = "response of get_user_location_by_session_id ";
		        $result .= json_encode($final_data);
		        //\Log::info(print_r($result, true)); 
		
                api_response(200,'',$final_data);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function get_check_in_list(Request $request){
        if(!empty($_REQUEST['user_id'])){
            try{
                $current_bash_arr = array();
				$user_id = $_REQUEST['user_id'];
				//$currenthosts =  Bash::where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'))->whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->get()->toarray();
				
                $current_bashs = Bash::whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'))->orderBy('start_date_time','asc')->get()->toarray(); 
                if(!empty($current_bashs)){
                    foreach($current_bashs as $current_bash){
                        $current_bash_arr[] = $this->get_bash_long($current_bash['id'],$_REQUEST['user_id']);
                    }
                }
                // /print_r($current_bash_arr); die;
                $upcomming_bash_arr = array();
                $upcomming_bashs = Bash::whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->where('end_date_time','<=', date('Y-m-d H:i:s') )->orderBy('start_date_time','asc')->get()->toarray();

                if(!empty($upcomming_bashs)){
                    foreach($upcomming_bashs as $upcomming_bash){
                        $upcomming_bash_arr[] = $this->get_bash_long($upcomming_bash['id'],$_REQUEST['user_id']);
                    }
                }
                api_response(200,'',array('live'=> $current_bash_arr, 'past'=> $upcomming_bash_arr ));
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function users_list(){
        $q = User::query();
        $q->where('id','!=',$_REQUEST['user_id']);
        $q->where('userType','=','1');
        $results = $q->orderBy('id','DESC')->get()->toArray();
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){
                $finalData[] = $this->nullvalue_filter($this->get_user($res['id']));
            }
        }
        if(!empty($finalData)){
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(404,'No data found');
        }
    }
    
	public function users_list_home(){
        $q = User::query();
        $q->where('id','!=',$_REQUEST['user_id']);
        //$q->where('updated_at','>=',date("Y-m-d H:i:s", strtotime("-36 hours")));
        $q->where('userType','=','1');
        $results = $q->orderBy('id','DESC')->get()->toArray();
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){
                $finalData[] = $this->nullvalue_filter($this->get_user($res['id']));
            }
        }
        if(!empty($finalData)){
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(404,'No data found');
        }
    }
	
	public function users_contact_list(){
        $q = User::query();
        $q->where('id','!=',$_REQUEST['user_id']);
		if(!empty($_REQUEST['phone'])){
			 $phones = explode(',',$_REQUEST['phone']);
			
		
			 //print_r($phones); die;
			$q->whereIn('phone_number',$phones);
		}else{
		    api_response(404,'There are no users registered on BASH from your contact list.');
		}
        //$q->where('updated_at','>=',date("Y-m-d H:i:s", strtotime("-36 hours")));
		
        $q->where('userType','=','1');
        $results = $q->orderBy('id','DESC')->get()->toArray();
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){
                $userData = $this->nullvalue_filter($this->get_user($res['id']));
				
				$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
				if(!empty($follow_status)){
					$userData['follow'] = 1;
				}else{
					$userData['follow'] = 0;
				}
                   
				$finalData[] = 	$userData;
            }
        }
        if(!empty($finalData)){
			 $finalData = Arr::sort($finalData, function($student)
			{
				return $student['fname'];
			});
			$finalData = array_values($finalData); 
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(404,'There are no users registered on BASH from your contact list.');
        }
    }
	
	public function users_contact_list_home(){
		$result = "request of users_contact_list_home ";
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true)); 
		$q = Follow::query();
        $q->where('user_id','=',$_REQUEST['user_id']);
		$curr_users = $this->nullvalue_filter($this->get_user($_REQUEST['user_id']));
        if(!empty($curr_users['block_users'])){
			//print_r($curr_users['block_users']); die;
			//$q->whereNotIn('other_id',explode(',', $curr_users['block_users']));
		}
		//$q->where('updated_at','>=',date("Y-m-d H:i:s", strtotime("-36 hours")));
        $results = $q->orderBy('id','DESC')->get()->toArray();
		
		//print_r($results); die;
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){ //print_r($res); die;
                $userData = $this->nullvalue_filter($this->get_user($res['other_id']));
				//print_r($userData); die;
				if($userData['location'] == '0' && $userData['last_location'] >= date("Y-m-d H:i:s", strtotime("-36 hours")) ){
					if(!empty($userData['block_users'])){
						//echo 
						$blockCheck = User::whereRaw('FIND_IN_SET('.$_REQUEST['user_id'].',block_users)')->where('id','=',$userData['id'])->count();
						
						if(!empty($blockCheck)){
							$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
							if(!empty($follow_status)){
								$userData['follow'] = 1;
							}else{
								$userData['follow'] = 0;
							}
							   
							$finalData[] = 	$userData;
						}
					}else{
						/*$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
						if(!empty($follow_status)){
							$userData['follow'] = 1;
						}else{
							$userData['follow'] = 0;
						}
						   
						$finalData[] = 	$userData; */
					}
					
				}else if($userData['location'] == '1' && $userData['last_location'] >= date("Y-m-d H:i:s", strtotime("-36 hours")) ){
					
						$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
						if(!empty($follow_status)){
							$userData['follow'] = 1;
						}else{
							$userData['follow'] = 0; 
						}
						   
						$finalData[] = 	$userData;
				}
			 }
        }
		$result = "response of users_contact_list_home ";
		//$result .= json_encode($finalData);
		//\Log::info(print_r($result, true)); 
        if(!empty($finalData)){
			 $finalData = Arr::sort($finalData, function($student)
			{
				return $student['fname'];
			});
			$finalData = array_values($finalData); 
            api_response(200,'User listed successfully',$finalData);
        }else{
			//updated (5 sep)
			$data = array();
            api_response(200,'No data found','','array');
        }
    }
	
	public function followed_user_list(){
		$q = Follow::query();
        $q->where('other_id','=',$_REQUEST['user_id']);
		$curr_users = $this->nullvalue_filter($this->get_user($_REQUEST['user_id']));
        if(!empty($curr_users['block_users'])){
			//print_r($curr_users['block_users']);
			//$q->whereNotIn('other_id',explode(',', $curr_users['block_users']));
		}
		//$q->where('updated_at','>=',date("Y-m-d H:i:s", strtotime("-36 hours")));
        $results = $q->orderBy('id','DESC')->get()->toArray();
		//print_r($results); die;
		
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){ //print_r($res); die;
                $userData = $this->nullvalue_filter($this->get_user($res['user_id']));
				//print_r($userData); die;
				//if(!empty($userData['location']) ){
					$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
					if(!empty($follow_status)){
						$userData['follow'] = 1;
					}else{
						$userData['follow'] = 0;
					}
					if(in_array($res['user_id'],explode(',', $curr_users['block_users']))){
						$userData['block'] = 1;
					}else{
						$userData['block'] = 0;
					}   
					$finalData[] = 	$userData;
				//}
				
            }
        }
        if(!empty($finalData)){
			$finalData = Arr::sort($finalData, function($student)
			{
				return $student['fname'];
			});
			$finalData = array_values($finalData); 
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(200,'No data found','','array');
        }
    }
	
	public function user_follow_both(){
		$q = Follow::query();
		$user_id  = $_REQUEST['user_id'];
		
		$q->Where(function ($query) use ($user_id) {
			$query->where('user_id', '=', $user_id)
			->orWhere('other_id', '=', $user_id);
		});
			
        $results = $q->orderBy('id','DESC')->get()->toArray();
		
        //$q->where('updated_at','>=',date("Y-m-d H:i:s", strtotime("-36 hours")));
		//print_r($results); die;
        $finalData = array();
        if(!empty($results)){
			$userrArr = array();
            foreach($results as $res){ //print_r($res); die;
			    
			    if($user_id != $res['other_id']  ){
					
					$userData = $this->nullvalue_filter($this->get_user($res['other_id']));
				     //print_r($userrArr);  
					if(!empty($userData) && !in_array($res['other_id'],$userrArr) ){
						$userrArr[] = $res['other_id'];
						$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
						if(!empty($follow_status)){
							$userData['follow'] = 1;
						}else{
							$userData['follow'] = 0;
						}
						   
						$finalData[] = 	$userData;
				   }
				}else{
					$userData = $this->nullvalue_filter($this->get_user($res['user_id']));
				    
					if(!empty($userData) && !in_array($res['user_id'],$userrArr)){
						$userrArr[] = $res['user_id'];
						$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
						if(!empty($follow_status)){
							$userData['follow'] = 1;
						}else{
							$userData['follow'] = 0;
						}
						   
						$finalData[] = 	$userData;
				   }
				}
                
            }
        }
        if(!empty($finalData)){
		    $finalData = Arr::sort($finalData, function($student)
			{
				return $student['fname'];
			});
			$finalData = array_values($finalData); 
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(404,'No data found');
        }
    }
	
	

	public function search_users(){
        $q = User::query();
		if(!empty($_REQUEST['search'])){
			$search = $_REQUEST['search'];
			
			$q->Where(function ($query) use ($search) {
                    $query->where('username', 'like', "%$search%")
                    ->orWhereRaw("concat(fname, ' ', lname) like '%".$search."%' ")
					->orWhereRaw("phone_number like '%".$search."%' ");
					
            });
		}else{
			api_response(200,'No data found','','array');
		}
        $q->where('id','!=',$_REQUEST['user_id']);
        $q->where('userType','=','1');
        $results = $q->orderBy('id','DESC')->get()->toArray();
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){ 
                $userData = $this->nullvalue_filter($this->get_user($res['id']));
				
				if(!empty($userData)){
					
					$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['id'] )->count();
					if(!empty($follow_status)){
						$userData['follow'] = 1;
					}else{
						$userData['follow'] = 0;
					}
					   
					$finalData[] = 	$userData;
				}
				
            }
        }
        if(!empty($finalData)){
			$finalData = Arr::sort($finalData, function($student)
			{
				return $student['fname'];
			});
			$finalData = array_values($finalData); 
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(200,'No data found','','array');
        }
    }
	
	public function search_bash(){
        $q = Bash::query();
		if(!empty($_REQUEST['search']) && !empty($_REQUEST['user_id']) ){
			$search = $_REQUEST['search'];
			$q->WhereRaw("name like '%".$search."%' ");
		}else{
			api_response(200,'No data found','','array');
		}
		
		$diff = 0;
		$user_data = $this->get_user($_REQUEST['user_id']);	
		if($user_data['dob'] == '0000-00-00'){
		}else{
		  $diff = (date('Y') - date('Y',strtotime($user_data['dob'])));
		}
		//$q->where('age','<=',$diff);	
		$q->where('start_date','<=', date('Y-m-d'));
        $q->where('end_date_time','>=', date('Y-m-d H:i:s'));
			
        $results = $q->orderBy('id','DESC')->get()->toArray();
		
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){  //print_r($res); die;
                $bashData = $this->nullvalue_filter($this->get_bash_long($res['id'],''));
				
				if(!empty($bashData)){
					if(in_array($_REQUEST['user_id'], explode(',',$bashData['name_of_host']))){
						$finalData[] = 	$bashData;
					}else{
						if(!empty($bashData['age_max'])){
							if($diff >= $res['age'] && $diff <= $res['age_max']){
								$finalData[] = 	$bashData;
							}
							
						}else{
							if($diff >= $res['age']){
								$finalData[] = 	$bashData;
							}
						}
					}
				}
				
            }
        }
        if(!empty($finalData)){
			$distance  = array_column($finalData, 'distance');
			array_multisort($distance, SORT_ASC, $finalData); 

            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(200,'No data found','','array');
        }
    }
	
	
    public function bash_users_list(){
        if(!empty($_REQUEST['bash_id'])){
            try{
                $bash_data = Bash::findorfail($_REQUEST['bash_id'])->toArray();
                
                $q1 = Ticket::query();
                $q1->where('bash_id','=',$_REQUEST['bash_id']);
                if(!empty($_REQUEST['user_id'])){
                    $q1->where('user_id','=',$_REQUEST['user_id']);
                }
                $results = $q1->orderBy('id','DESC')->get()->toArray();
                //print_r($results); die;
                $finalData = array();
                if(!empty($results)){
                    foreach($results as $resul){
                        // get bar code data
                        $q2 = Qrcode::query();
                        $q2->where('ticket_id','=',$resul['id']);
                        $resultss = $q2->orderBy('id','DESC')->get()->toArray();
                        $barcodedata = array();
                        if(!empty($resultss)){
                            foreach($resultss as $red){
                                //$red['used'] = '0';
                                $barcodedata[] = $red;
                            }
                        }
                        $resul['barcodedata'] = $barcodedata;
						$ususe = $this->get_user($resul['user_id']);
						if(!empty($bash_data['name_of_host'])){
							$hostdatas = explode(',', $bash_data['name_of_host']);
							    if(in_array($hostd, $hostdatas)){
									$ususe['is_host'] = '1';
								}else{
									$ususe['is_host'] = '0';
								}
						}else{
							$ususe['is_host'] = '0';
						}
				        $ususe['check_in'] = '1';
						$ususe['bash_price'] = $bash_data['charge'];
                        $finalData[] = array('ticket'=> $resul, 'user_data'=> $ususe);
                    }
                }else{
                    //$bash_data['ticket_data'] =  array();
                }
                
                if(!empty($bash_data['name_of_host'])){
                    $hostdatas = explode(',', $bash_data['name_of_host']);
                    foreach($hostdatas as $hostd){
                        $user_dd = $this->get_user($hostd);
						if(in_array($hostd, $hostdatas)){
							$user_dd['is_host'] = '1';
						}else{
							$user_dd['is_host'] = '0';
						}
						$user_dd['check_in'] = '1';
						$user_dd['bash_price'] = $bash_data['charge'];
                        if(!empty($user_dd)){
                            $finalData[] = array('ticket'=> new \stdClass(), 'user_data'=> $user_dd);
                        }
                    }
                } 
                
                $checkin_datas =  Crash::where('bash_id', '=', $_REQUEST['bash_id'])->get()->toArray();

                if(!empty($checkin_datas)){
                    $hostdatas = explode(',', $bash_data['name_of_host']);
                    foreach($checkin_datas as $checkin_dat){
                        $user_dd_check = $this->get_user($checkin_dat['user_id']);
						if(in_array($checkin_dat->user_id, $hostdatas)){
							$user_dd_check['is_host'] = '1';
						}else{
							$user_dd_check['is_host'] = '0';
						}
						// check checkin
						$checkin_datas =  Checkin::where('skip', '=', '0')->where('barcode', '=', '')->where('bash_id', '=', $_REQUEST['bash_id'])->where('user_id', '=', $checkin_dat['user_id'])->get()->toArray();
						if(!empty($checkin_datas)){
							$user_dd_check['check_in'] = '1';
						}else{
							$user_dd_check['check_in'] = '0';
						}
						$user_dd_check['bash_price'] = $bash_data['charge'];
                        if(!empty($user_dd)){
                            $finalData[] = array('ticket'=> new \stdClass(), 'user_data'=> $user_dd_check);
                        }
                    }
                }

                //print_r($checkin_datas); die;
                api_response(200,'Data listed successfully',$finalData);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'No bash found');
            }
        }else{
            api_response(404,'Please fill all required data');
        }

        $q = User::query();
        $q->where('id','!=',$_REQUEST['user_id']);
        $q->where('userType','=','1');
        $results = $q->orderBy('id','DESC')->get()->toArray();
        $finalData = array();
        if(!empty($results)){
            foreach($results as $res){
                $finalData[] = $this->nullvalue_filter($this->get_user($res['id']));
            }
        }
        if(!empty($finalData)){
            api_response(200,'User listed successfully',$finalData);
        }else{
            api_response(404,'No data found');
        }
    }
    
    public function get_bash_details(){
		
        if(!empty($_REQUEST['bash_id'])){
            $bash_id = $_REQUEST['bash_id'];
			
            $bash_data = $this->get_bash_long($bash_id,'');
            if(!empty($bash_data)){ // >=
				$bashExire =  Bash::where('id','=', $bash_id)->where('end_date_time','>', date('Y-m-d H:i:s'))->get()->toarray();
				//echo date('Y-m-d H:i:s');
			   
				if(!$bashExire){
					api_response(200,'Event has expired.');
				}
				// print_r($bash_data); die;
                api_response(200,'Data listed successfully',$bash_data);
            }else{
                api_response(200,'This event no longer exsits.');
            } 
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
	public function face_color_list(Request $request){
        if(!empty($_REQUEST['type'])){
            try{
                $final_data = array();
                $faces = Face::select($_REQUEST['type'])->distinct()->get()->toArray();
				api_response(200,'',$faces);
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        }else{
            api_response(404,'Please fill all required data');
        }
    }
	
	public function face_match(Request $request){
        /* if(!empty($_REQUEST['face_color'])){ */
			$face_color = $_REQUEST['face_color'];
			$eye_color = $_REQUEST['eye_color'];
			$eyebrow_color = $_REQUEST['eyebrow_color'];
			$beared = $_REQUEST['beared'];
			$gender = $_REQUEST['gender'];
			
			$match_arr = array();
            try{
                $faces = Face::get()->toArray();
				$index = 0;
				foreach($faces as $face){
					$match = 0;
					if($face['face_color'] == $face_color){
						$match = $match+1;
					}
					if($face['gender'] == $gender){
						$match = $match+1;
					}
					if($face['beared'] == $beared){
						$match = $match+1;
					}
					if($face['eyebrow_color'] == $eyebrow_color){
						$match = $match+1;
					}
					if($face['eye_color'] == $eye_color){
						$match = $match+1;
					}
					$match_arr[] = array('index'=> $index,'match'=> $match,'id'=> $face['id']);
					//print_r($match_arr);
					$index++;
				}
                if(!empty($match_arr)){
					$match = array_column($match_arr, 'match');
					array_multisort($match, SORT_DESC, $match_arr); 
					api_response(200,'',$faces[$match_arr[0]['index']]);
				}else{
					api_response(404,'No face match found');
				}
    
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        /* }else{
            api_response(404,'Please fill all required data');
        } */
    }
	
    public function get_user1($other_id,$user_id=null,$size=null){
        if(!empty($user_id)){
              try{
                  $user = User::findorfail($user_id)->toarray();
                  $user['today_bash_count'] = $this->get_crash_count_today($user_id);
                  $user['lat'] = !empty($user['lat'])?$user['lat']:'0';
                  $user['lng'] = !empty($user['lng'])?$user['lng']:'0';
                  $user['last_location_text'] = !empty($user['lat'])?time_elapsed_string($user['last_location']):'Not updated yet';

                  $user['hosted'] = Bash::whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->count();
                  $user['attendant'] = 0;
                  $user['followes'] = Follow::where('other_id','=',$user_id )->count();
                  $user['following'] = Follow::where('user_id','=',$user_id )->count();
                  $user = $this->nullvalue_filter($user);
                  if(!empty($size)){
                     
                    
                      if(!empty($other_id)){
                          $follow_status = Follow::where('user_id','=',$other_id )->where('other_id','=',$user_id )->count();
                          if(!empty($follow_status)){
                              $user['follow'] = 1;
                          }else{
                              $user['follow'] = 0;
                          }
                      }else{
                          $user['follow'] = 0;
                      }
                      $user = $this->nullvalue_filter($user);
                      $q = Bash::query();
                      $q->where('user_id','=',$user_id);
                      $results = $q->orderBy('id','DESC')->offset(0)->limit(20)->get()->toArray();
                      $recent_bash = array();
                      if(!empty($results)){
                          foreach($results as $result){
                              $recent_bash[] = $this->get_bash($result['id']);
                          }
						  $start_date_time  = array_column($recent_bash, 'start_date_time');
						  array_multisort($start_date_time, SORT_ASC, $recent_bash);
						}
                      $user['recent_bash'] = $recent_bash;
                  }
				  $bash_rate_data = $this->get_bash_rate($user_id);
				  $user['male'] = $bash_rate_data['male'];
				  $user['fe_male'] = $bash_rate_data['fe_male'];
				  $user['not_specified'] = $bash_rate_data['not_specified'];
				  $user['ratings'] = $bash_rate_data['ratings'];
                  $user['complement'] = $bash_rate_data['complement'];
                  $user['complement1'] = $bash_rate_data['complement1'];
                  $user['complement2'] = $bash_rate_data['complement2'];
                  $user['complement3'] = $bash_rate_data['complement3'];
                  $user['complement4'] = $bash_rate_data['complement4'];
                  $user['complement5'] = $bash_rate_data['complement5'];
					
                 return $user;
              }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                  api_response(404,'Error in server');
              } 
          }else{
              api_response(404,'Error in server');
          }
      }

    public function get_user($user_id,$other_id=null,$size=null){
      if(!empty($user_id)){
            try{
                $user = User::findorfail($user_id)->toarray();
                $user['today_bash_count'] = $this->get_crash_count_today($user_id);
                $user['lat'] = !empty($user['lat'])?$user['lat']:'0';
                $user['lng'] = !empty($user['lng'])?$user['lng']:'0';
                $user['last_location_text'] = !empty($user['lat'])?time_elapsed_string($user['last_location']):'Not updated yet';
                $user['hosted'] = Bash::whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->count();
                $user['attendant'] = 0;
                $user['followes'] = Follow::where('other_id','=',$user_id )->count();
                $user['following'] = Follow::where('user_id','=',$user_id )->count();
				$user['today_bash'] = Bash::where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'))->whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->count();
				 
                $user = $this->nullvalue_filter($user);
                if(!empty($size)){
                   

                    if(!empty($other_id)){
                        $follow_status = Follow::where('user_id','=',$user_id )->where('other_id','=',$other_id )->count();
                        if(!empty($follow_status)){
                            $user['follow'] = 1;
                        }else{
                            $user['follow'] = 0;
                        }
                    }else{
                        $user['follow'] = 0;
                    }
                    
                    
                    $bash_q = Bash::query();
                    $bash_q->where('user_id','=',$other_id);
                    $results = $bash_q->orderBy('id','DESC')->offset(0)->limit(20)->get()->toArray();
                    $recent_bash = array();
                    if(!empty($results)){
                        foreach($results as $result){
                            $recent_bash[] = $this->get_bash($result['id']);
                        }
						$start_date_time  = array_column($recent_bash, 'start_date_time');
						array_multisort($start_date_time, SORT_ASC, $recent_bash);
                    }
                    $user['recent_bash'] = $recent_bash;

                    // get notify bash
                    $free_notify = array();
                    //echo date('Y-m-d H:i:s'); die;
                    $bash_ddtas = Bash::where('end_date_time','>=', date('Y-m-d H:i:s'))->whereRaw('FIND_IN_SET('.$user_id.',free_notify)')->get()->toArray();
                    //print_r($bash_ddtas); die;
                    if(!empty($bash_ddtas)){
                        foreach($bash_ddtas as $bash_ddta){ 
                            // check is it check-in or not
                           $checkinExsists =  Checkin::where('user_id', '=', $user_id)->where('bash_id', '=', $bash_ddta['id'])->first();
						   
                           if(!empty($checkinExsists)){

                           }else{
                            
                            $bash_data_final = $this->get_bash($bash_ddta['id']);
                            if(!empty($bash_data_final)){
                                $free_notify[] = $bash_data_final;
                            }

                           }
                            
                        }
                    }
                    $user['free_notify'] = $free_notify;
                    //echo "hi"; die;
                    // get pending rating
                    $pendingRating = array();  
                    $past_bashes =  Bash::where('end_date_time','<=', date('Y-m-d H:i:s'))->get()->toArray();
                    if(!empty($user['pending_rating'])){
                        $bash_pending_ratings = explode(',',$user['pending_rating']);
                        foreach($bash_pending_ratings as $bash_pending_rating){ 
                            // check is it check-in or not
                           $checkinExsists =  Rating::where('user_id', '=', $user_id)->where('bash_id', '=', $bash_pending_rating)->first();
                           if(!empty($checkinExsists)){

                           }else{
                                $bash_data_final = $this->get_bash($bash_pending_rating);
                                if(!empty($bash_data_final)){
                                    $user_data = $this->get_user($bash_data_final['user_id']);
                                    $bash_data_final['user_data'] =  array('id'=>$user_data['id'], 'fname'=>$user_data['fname'], 'lname'=>$user_data['lname'], 'username'=>$user_data['username'] , 'image'=>$user_data['image']);
                                    $pendingRating[] = $bash_data_final;
                                }
                            }
                            
                        }
                    } 
                    
                    $user['pendingRating'] = $pendingRating;
                    
					$bash_rate_data = $this->get_bash_rate($user_id);
					$user['male'] = $bash_rate_data['male'];
					$user['fe_male'] = $bash_rate_data['fe_male'];
					$user['not_specified'] = $bash_rate_data['not_specified'];
					$user['ratings'] = $bash_rate_data['ratings'];
                    $user['complement'] = $bash_rate_data['complement'];
                    $user['complement1'] = $bash_rate_data['complement1'];
                    $user['complement2'] = $bash_rate_data['complement2'];
                    $user['complement3'] = $bash_rate_data['complement3'];
                    $user['complement4'] = $bash_rate_data['complement4'];
                    $user['complement5'] = $bash_rate_data['complement5'];
                      
                    $safeRide =  SafeRide::where('user_id', '=', $user_id)->first();
                    $safeData = array();

                    if(!empty($safeRide)){
                        $safeUsers = array();
                        if(!empty($safeRide->users)){
                        $hostdatas = explode(',', $safeRide->users);
                        
                        foreach($hostdatas as $hostd){
                            $user_dd = $this->get_user($hostd);
                            if(!empty($user_dd)){
                                $safeUsers[] = array('id'=>$user_dd['id'], 'fname'=>$user_dd['fname'], 'lname'=>$user_dd['lname'], 'username'=>$user_dd['username'] , 'image'=>$user_dd['image']);
                            }
                            
                        }}
                        $safeData = array('id'=> $safeRide->id, 'users'=> $safeUsers , 'location'=> $safeRide->location , 'lat'=> $safeRide->lat , 'lng'=> $safeRide->lng , 'notify'=> $safeRide->notify );
                    } 

                    $user['safe_ride'] = !empty($safeData)?$safeData:new \stdClass();
                    $pending_wallet =  TempWallet::where('user_id', '=', $user_id)->orderBy('id','DESC')->first();
                    $user['temp_wallet'] = !empty($pending_wallet->total_amount) ?$pending_wallet->total_amount:'0.00';
                    
                    $wallet =  Wallet::where('user_id', '=', $user_id)->orderBy('id','DESC')->first();
                    $user['wallet'] = !empty($wallet->total_amount) ?$wallet->total_amount:'0.00';

                    // get user wallet
                    //$user['temp'] = 
                }
               return $user;
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                return array();
            } 
        }else{
            return array();
        }
    }
    
    public function follow(Request $request){
        	$result = "request of follow ";
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true)); 
		
        if(!empty($_REQUEST['user_id'])  && !empty($_REQUEST['other_id']) && isset($_REQUEST['status']) ){
            try {
                if($_REQUEST['status'] == '0'){
                    $follow =  Follow::where('user_id', '=', $request->user_id )->where('other_id', '=', $request->other_id)->get()->toarray();
                    //print_r($follow); die;
                    if(!empty($follow[0]['id'])){
                        Follow::where('id','=',$follow[0]['id'] )->delete();
                    }
                    
                    api_response(200,'Unfollowed successfully',$this->get_user1($_REQUEST['user_id'],$_REQUEST['other_id'],'1'));
                }
                

                $follow =  Follow::where('user_id', '=', $request->user_id )->where('other_id', '=', $request->other_id)->count();
                if(!empty($follow)){
                   
                    api_response(200,'Followed successfully',$this->get_user1($_REQUEST['user_id'],$_REQUEST['other_id'],'1'));
                }else{
                    $follow = new Follow();
                    $follow->user_id = $request->user_id;
                    $follow->other_id = $request->other_id;
                    $follow->save();
                    $user = $this->get_user($request->user_id);

                    $mes = $user['fname']." ".$user['lname']." started following you";
                    $this->save_notify($request->other_id,'New Follower', $mes,'follow',$request->user_id);
					$result = "response of follow ";
					//$result .= json_encode($this->get_user1($_REQUEST['user_id'],$_REQUEST['other_id'],'1'));
					//\Log::info(print_r($result, true)); 
                    api_response(200,'Followed successfully',$this->get_user1($_REQUEST['user_id'],$_REQUEST['other_id'],'1'));
                }
                
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function get_bash($id){
        if(!empty($id)){
              try{
                  $data = Bash::findorfail($id)->toarray();
                  return $this->nullvalue_filter($data);;
              }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                  return array();
              } 
          }else{
              return array();
          }
    }
    
    public function followers_list(){
        if(!empty($_REQUEST['user_id'])){
            $q = Follow::query();
            $q->where('other_id','=',$_REQUEST['user_id']);
            
            $results = $q->orderBy('id','DESC')->get()->toArray();
            $finalData = array();
            if(!empty($results)){
                foreach($results as $res){
                    $finalData[] = $this->nullvalue_filter($this->get_user($res['user_id'],'','1'));
                }
            }
            if(!empty($finalData)){
				$finalData = Arr::sort($finalData, function($student)
				{
					return $student['fname'];
				});
			    $finalData = array_values($finalData); 
                api_response(200,'Data listed successfully',$finalData);
            }else{
                api_response(200,'No data found','','array');
            }
        }else{
            api_response(404,'Please fill all required data');
        }
        
    }

    public function generateToken(){
       
        require "public/braintree/lib/Braintree.php";

          /* $gateway = new \Braintree_Gateway([
             'environment' => 'sandbox',
             'merchantId' => 't8wv7sd2vz2mk9bj',
             'publicKey' => '59v3xxnqnt7rrr3g',
             'privateKey' => 'dd5c73d100479efcb46f67a957aad7e1'
         ]);  
         */
        $gateway = new \Braintree_Gateway([
               'environment' => 'production',
               'merchantId' => 'mvtf4n87ff53nwcp',
               'publicKey' => '49vzfbyx95dbcfv3',
               'privateKey' => '503deea5ef0e499e49edb0003f7bab45'
            ]);
         $clientToken = $gateway->clientToken()->generate();
        if(!empty($clientToken)){
            api_response(200,'Token generated succesfully.',array('token'=>$clientToken ));
        }
        else {
            api_response(404,'There is some issue occured payment, please contact to admin if problem persist.');
        }
        a:
        echo json_encode($results);
        exit;
  }
    
    public function make_payment($price=null,$paymentToken=null){

        require "public/braintree/lib/Braintree.php";
          
			
         /*$gateway = new \Braintree_Gateway([
             'environment' => 'sandbox',
             'merchantId' => 't8wv7sd2vz2mk9bj',
             'publicKey' => '59v3xxnqnt7rrr3g',
             'privateKey' => 'dd5c73d100479efcb46f67a957aad7e1'
         ]);*/
		 $gateway = new \Braintree_Gateway([
               'environment' => 'production',
               'merchantId' => 'mvtf4n87ff53nwcp',
               'publicKey' => '49vzfbyx95dbcfv3',
               'privateKey' => '503deea5ef0e499e49edb0003f7bab45'
        ]);
        if(!empty($_REQUEST['device_data'])){
			$result = $gateway->transaction()->sale([
				'amount' => $price,
				'paymentMethodNonce' => $paymentToken,
				'options' => [ 'submitForSettlement' => true ],
				'deviceData' => $_REQUEST['device_data']
			]);
		}else{
			$result = $gateway->transaction()->sale([
				'amount' => $price,
				'paymentMethodNonce' => $paymentToken,
				'options' => [ 'submitForSettlement' => true ]
			]);
		}
        
        
        if ($result->success) {
            return $result->transaction->id;
        }else if ($result->transaction) {
            return false;
        }else{
            return false;
        }
    }

    public function save_notify($user_id, $title, $mess, $type,$bash_id=null){
        $user = $this->get_user($user_id);
        //print_r($user);die;
        if($user['device_type'] == 'android' && !empty($user['device_token'])){
            send_notification($user['device_token'], $title, $mess, $type,$bash_id );
        }
        if($user['device_type'] == 'ios' && !empty($user['device_token'])){
            send_inotification($user['device_token'], $title, $mess, $type,$bash_id );
        }
    }

    public function redirect($id){
        
        return view('redirect')->with('id', $id);
    }
    
	public function redirect1($id){
        
        return view('redirect1')->with('id', $id);
    }
	
    public function get_crash_count_today($user_id){
		
        if(!empty($user_id)){
			  try{
				
                $current_bash_arr = $current_bash_id_arr = array();
				
				$currenthosts =  Bash::where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'))->whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->get()->toarray();
                if(!empty($currenthosts)){
                    foreach($currenthosts as $curren){
                        $current_bash_id_arr[] = $curren['id'];
                    }
                }
				
				$current_bashs_ticket = Ticket::where('user_id', '=', $user_id )->whereHas('bash', function ($query) {
                    $query->where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'));
                })->get()->toarray();
                
                if(!empty($current_bashs_ticket)){
                    foreach($current_bashs_ticket as $current_bashs_ti){ 
                        $current_bash_id_arr[] = $current_bashs_ti['bash_id'];
                    }
                }
				
                

                $current_bashs = Crash::where('user_id', '=', $user_id)->whereHas('bash', function ($query) {
                    $query->where('start_date','<=', date('Y-m-d'))->where('end_date_time','>=', date('Y-m-d H:i:s'));
                })->get()->toarray();
                
                if(!empty($current_bashs)){
                    foreach($current_bashs as $current_bash){ 
                        $current_bash_id_arr[] = $current_bash['bash_id'];
                    }
                }

                return count($current_bash_id_arr);
              }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                  api_response(404,'Error in server');
              } 
          }else{
              api_response(404,'Error in server');
          }
    }
    
    public function buy_ticket(Request $request){
        
        if(!empty($_REQUEST['user_id'])  && !empty($_REQUEST['bash_id']) && isset($_REQUEST['qty']) && !empty($_REQUEST['amount']) && !empty($_REQUEST['type']) ){
            try {
                $new_count =  Ticket::where('user_id', '=', $request->user_id )->where('bash_id', '=', $request->bash_id)->count();
                $bash_data =  Bash::where('id', '=', $request->bash_id)->first();
                $user_data =  User::where('id', '=', $request->user_id)->first();
                if(!empty($new_count)){
                    api_response(404,'You have already purchased tickets for this event');
                }
                if($_REQUEST['type'] == '5'){
                    // validate wallet
                    $wallet =  Wallet::where('user_id', '=', $_REQUEST['user_id'])->orderBy('id','DESC')->first();
                    if($wallet->total_amount < $_REQUEST['amount']){
                        api_response(404,'You have insufficient funds.');
                     }else{
                        
                    }
                }

                $ticket = new Ticket();
                $ticket->user_id = $request->user_id;
                $ticket->bash_id = $request->bash_id;
                $ticket->amount = $request->amount;
                $ticket->qty = $request->qty;
                $total_wallet = '0';
				$payment_status = '';
                if($_REQUEST['type'] != '5'){
                    $payment_status = $this->make_payment($request->amount, $request->token);
                    if(!$payment_status){
                        api_response(404,'Error in payment.');
                    }
                }else{
					$this->add_wallet($request->user_id,$_REQUEST['amount'],'debit',null,'','','');
				}
                $wallet =  Wallet::where('user_id', '=', $request->user_id)->orderBy('id','DESC')->first();
				$total_wallet = !empty($wallet->total_amount)?$wallet->total_amount:'0.00';
                $ticket->type = $request->type;
                $ticket->transaction_id = $payment_status;
                $ticket->save();
                $this->add_temp_wallet($bash_data->user_id,$request->user_id,$request->amount,'credit',$request->bash_id);
                for ($x = 1; $x <= $request->qty; $x++) {
                    $this->add_qr_code($ticket->id,$request->bash_id,$request->user_id);
                }
                $mes = $user_data->fname.' '.$user_data->lname." has purchased ".$_REQUEST['qty']." ticket for your BASH, '".$bash_data->name."'";
                //echo $mes; die;
                $this->save_notify($bash_data->user_id,'Ticket Purchased',$mes,'buy_ticket');

                api_response(200,'Ticket has been purchased successfully',array('wallet'=> $total_wallet, 'count'=> $this->get_crash_count_today($_REQUEST['user_id'])));
                
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    
    public function barcode_check_in(Request $request){
        
        if(!empty($_REQUEST['code']) && !empty($_REQUEST['bash_id']) ){
            try {
                
                $checkin =  Qrcode::where('code', '=', $request->code)->where('bash_id', '=', $request->bash_id)->first();
				$bash_data =  Bash::where('id', '=', $request->bash_id)->first();
                if(!empty($checkin)){
					if($checkin->status == '1'){
						api_response(200,'This ticket has been used');
					}
                    $checkin->status = '1';
                    $checkin->save();
                    $checkin_new = new Checkin();
                    $checkin_new->barcode = $checkin->code;
					$ticket_data =  Ticket::where('id', '=', $checkin->ticket_id)->first();
                    $checkin_new->user_id = $ticket_data->user_id;
                    $checkin_new->bash_id = $checkin->bash_id;
					$checkin_new->bash_user_id = $bash_data->user_id;
                    $checkin_new->save();
					api_response(200,'Barcode has been used successfully');
                    
                }else{
                    api_response(200,'Invalid barcode');
                }
    
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function free_check_in(Request $request){
        
        if(!empty($_REQUEST['user_id']) && !empty($_REQUEST['bash_id']) && isset($_REQUEST['skip']) ){
            try {
                
                $checkin =  Checkin::where('user_id', '=', $request->user_id)->where('bash_id', '=', $request->bash_id)->first();
                $bash_data =  Bash::where('id', '=', $request->bash_id)->first();
                $user_data =  User::where('id', '=', $request->user_id)->first();
                if(!empty($checkin)){
					if($checkin->skip == '1'){
						api_response(200,'Skipped successfully');
					}else{
						api_response(404,'Already checked in');
					}
                }else{
                    $checkin_new = new Checkin();
                    $checkin_new->barcode  = '';
                    $checkin_new->user_id = $request->user_id;
                    $checkin_new->bash_id = $request->bash_id;
                    $checkin_new->skip = $request->skip;
					$checkin_new->bash_user_id = $bash_data->user_id;
                    $checkin_new->save();
                   
                    if($_REQUEST['skip'] == '1'){
                        api_response(200,'Skipped successfully');
                    }else{
                        $mes = $user_data->fname.' '.$user_data->lname." has checked into your event ".$bash_data->name;
                        //echo $mes; die;
                        $this->save_notify($bash_data->user_id,'Checked in',$mes,'check_in');
                        api_response(200,'Checked in successfully');
                    }
                }
    
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function give_rate(Request $request){
        
        if(!empty($_REQUEST['user_id']) && !empty($_REQUEST['bash_id']) && isset($_REQUEST['skip']) && isset($_REQUEST['rating']) && isset($_REQUEST['complement']) ){
            try {
                $bash_data =  Bash::where('id', '=', $request->bash_id)->first();
                $user_data =  User::where('id', '=', $request->user_id)->first();

                $checkin =  Rating::where('user_id', '=', $request->user_id)->where('bash_id', '=', $request->bash_id)->first();
                if(!empty($checkin)){
                    api_response(404,'Already Rated');
                }else{
                    $rating = new Rating();
                    $rating->user_id = $request->user_id;
                    $rating->bash_id = $request->bash_id;
                    $rating->skip = $request->skip;
                    $rating->rating = $request->rating;
                    $rating->bash_user_id = $bash_data->user_id;

                    if(!empty($request->complement)){
                        $rating->complement = $request->complement;
                    }else{
                        $rating->complement = '';
                    }
                    
                    $rating->save();
					if($_REQUEST['skip'] != '1'){
						$mes = $user_data->fname.' '.$user_data->lname." gave ".$request->rating."/3 star to ".$bash_data->name;
                        $this->save_notify($bash_data->user_id,'Event Rating',$mes,'rated');
                        api_response(200,'You have rated in successfully');
					}else{
                        api_response(200,'You have skipped this rating.');
                    }
                    
                  }
    
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function withdraw_money(Request $request){
        require "public/paypal_payout/vendor/autoload.php";

        if(!empty($_REQUEST['user_id'])  && !empty($_REQUEST['email']) ){

            $user_id = $_REQUEST['user_id'];
            $wallet =  Wallet::where('user_id', '=', $user_id)->orderBy('id','DESC')->first();
            if(!empty($wallet->total_amount)){ }else{
                api_response(404,'Withdraw amount is not valid');
            }
			
            $amount = !empty($wallet->total_amount) ?$wallet->total_amount:'0.00';
            $email = $_REQUEST['email'];

            /*$apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    'AeZJFulZsHdO-HCJFRrDscpZ8Or7cN8PakwuTNUSdvi9dVo1h6KCOQD_vUftVE2kUXD13GgVvUQTNGkl',     // ClientID
                    'EHtAirUF1n3E6zGVOiDXb6rCls04rlOZt3kjrKg-p2thr8q2U2poBfN2e0aK6yM4IIP68HmvaXUgdJ4O'      // ClientSecret
                )
            );*/
            
			  $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    'AajbMuzyIRMlfOqWgztSvi8WgtBwwBTYI4aWA4C06CcdJOV3FRE5qNN_9h6zAL0UTObo8Lm9S2JFAWPS',     // ClientID
                    'EE_6kmbI-WegHEkMF_fdxHwIhn7BZgnfzZjtXb8e0KoUNm5zzZ3UUDA3XeBtAcSVWVNS8oQYw3H3adZs'      // ClientSecret
                )
            );
            $apiContext->setConfig(
				  array(
					'mode' => 'live',
					)
			);
			
            $payouts = new \PayPal\Api\Payout();
            $senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
            $senderBatchHeader->setSenderBatchId(uniqid().microtime(true))
            ->setEmailSubject("You have a make a withraw request");
                
            $senderItem = new \PayPal\Api\PayoutItem();
                    $senderItem->setRecipientType('Email')
                        ->setNote('Thanks you.')
                        ->setReceiver($email)
                        ->setSenderItemId("item_1" . uniqid().microtime('true'))
                        ->setAmount(new \PayPal\Api\Currency('{
                    "value":'.$amount.',
                    "currency":"USD"
                }'));
                $payouts->setSenderBatchHeader($senderBatchHeader)->addItem($senderItem);
                $request = clone $payouts;

            //$output = $payouts->create(null, $apiContext);
            //die;  
            

            try {
                $output = $payouts->create(null, $apiContext);
				//print_r($output); die;
                $payoutBatchId = $output->getBatchHeader()->getPayoutBatchId();
                
                // $user =  User::where('id', '=', $user_id)->first();
                // $user->paypal_id = $email;
                // $user->save();
                $this->add_wallet($user_id,$amount,'debit',null,$payoutBatchId,'PENDING','');
                api_response(200,'Amount withdraw successfully.');
            } catch (\Exception $ex) {
				$error_data = json_decode($ex->getData());
				if(!empty($error_data->message)){
					api_response(404,$error_data->message);
				}else{
					api_response(404,'Error in payment, please try after sometime');
				}
			}

        }else{
            api_response(404,'Please fill all required data');
        }
    }
     
    public function search(Request $request){
        
        if( !empty($_REQUEST['user_id'])){
            try {
                $search = $_REQUEST['text'];
                if(!empty($search)){
                    $datas = User::where('id', '!=', $_REQUEST['user_id'] )
                
                    ->Where(function ($query) use ($search) {
                        $query->where('username', 'like', "%$search%")
                            ->orWhereRaw("concat(fname, ' ', lname) like '%".$search."%' ")
                            ;
                    })->get()->toArray();
                }else{
                    api_response(200,'No data found','','array');
                }
               

               
                
                //print_r($datas); die;
                if(!empty($datas)){
                    $finalData = array();
                      foreach($datas as $data){ 
                        $finalData[] = array('id'=> $data['id'],'fname'=> $data['fname'],'lname'=> $data['lname'],'username'=> $data['username']);
                      }
                      api_response(200,'Data listed successfully',$finalData);
                }else{
                    api_response(200,'No data found','','array');
                }
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }

    public function add_ride(Request $request){
        
        if(!empty($_REQUEST['user_id']) && isset($_REQUEST['users']) && !empty($_REQUEST['location']) && !empty($_REQUEST['lat']) && !empty($_REQUEST['lng']) && isset($_REQUEST['notify']) ){
            try {
                $rides =  SafeRide::where('user_id', '=', $request->user_id)->first();
                
                if(!empty($rides)){
                }else{
                    $rides = new SafeRide();
                }
                $rides->user_id = $request->user_id;
                $rides->users = !empty($request->users)?$request->users:'';
                $rides->location = $request->location;
                $rides->lat = $request->lat;
                $rides->lng = $request->lng;
                $rides->notify = $request->notify;
                $rides->save();
                api_response(200,'Ride added successfully',$this->get_user($_REQUEST['user_id'],'','1'));

            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }
	
	public function weekly_payment_details(Request $request){
        
        if(!empty($_REQUEST['user_id']) && !empty($_REQUEST['date'])){
			
			
			$user_id= $_REQUEST['user_id'];
			$currDate = $_REQUEST['date'];
			$bashes =  Bash::where('end_date','>=',date('Y-m-d',strtotime($currDate .'-6days')))->where('end_date','<=', date('Y-m-d',strtotime($currDate)))->whereRaw('FIND_IN_SET('.$user_id.',name_of_host)')->orderBy('end_date_time','ASC')->get()->toarray();
			
		    
			$earning = array(
			     array('date'=> date('Y-m-d',strtotime($currDate .'-6 days')), 'day'=> date('D',strtotime($currDate .'-6 days')), 'earings' => '0.00'),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-5 days')), 'day'=> date('D',strtotime($currDate .'-5 days')), 'earings' => '0.00'),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-4 days')), 'day'=> date('D',strtotime($currDate .'-4 days')), 'earings' => '0.00'),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-3 days')), 'day'=> date('D',strtotime($currDate .'-3 days')), 'earings' => '0.00'),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-2 days')), 'day'=> date('D',strtotime($currDate .'-2 days')), 'earings' => '0.00'),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-1 days')), 'day'=> date('D',strtotime($currDate .'-1 days')), 'earings' => '0.00'),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-0 days')), 'day'=> date('D',strtotime($currDate .'-0 days')), 'earings' => '0.00'),
			);
			
			$attendance = array(
			     array('date'=> date('Y-m-d',strtotime($currDate .'-6 days')), 'day'=> date('D',strtotime($currDate .'-6 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-5 days')), 'day'=> date('D',strtotime($currDate .'-5 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-4 days')), 'day'=> date('D',strtotime($currDate .'-4 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-3 days')), 'day'=> date('D',strtotime($currDate .'-3 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-2 days')), 'day'=> date('D',strtotime($currDate .'-2 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-1 days')), 'day'=> date('D',strtotime($currDate .'-1 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
				 array('date'=> date('Y-m-d',strtotime($currDate .'-0 days')), 'day'=> date('D',strtotime($currDate .'-0 days')), 'male' => 0,'fe_male' => 0,'not_specified' => 0),
			);
			$bashArr = [];
			if(!empty($bashes)){
				if(!empty($bashes)){
					foreach($bashes as $bash){
						$bash_ids[] = $bash['id'];
						$bsh_data = $this->get_bash_long($bash['id'],'');
						$bashArr[] = $bsh_data;
						//print_r($bsh_data);
						
						//print_r($wallet);
						if(date('Y-m-d',strtotime($currDate .'-6 days')) == $bash['end_date']){
							//$amount_tax = ($bash['charge']-($bash['charge']*6.5)/100);
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							if(!empty($wallet))
							$earning[0]['earings'] += number_format($wallet,2);
						}
						if(date('Y-m-d',strtotime($currDate .'-5 days')) == $bash['end_date']){
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							
							if(!empty($wallet))
							$earning[1]['earings'] += number_format($wallet,2);
						     
						}
						if(date('Y-m-d',strtotime($currDate .'-4 days')) == $bash['end_date']){
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							if(!empty($wallet))
							$earning[2]['earings'] += number_format($wallet,2);
						}
						if(date('Y-m-d',strtotime($currDate .'-3 days')) == $bash['end_date']){
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							if(!empty($wallet))
							$earning[3]['earings'] += number_format($wallet,2);
						}
						if(date('Y-m-d',strtotime($currDate .'-2 days')) == $bash['end_date']){
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							if(!empty($wallet))
							$earning[4]['earings'] += number_format($wallet,2);
						}
						if(date('Y-m-d',strtotime($currDate .'-1 days')) == $bash['end_date']){
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							if(!empty($wallet))
							$earning[5]['earings'] += number_format($wallet,2);
						}
						if(date('Y-m-d',strtotime($currDate .'-0 days')) == $bash['end_date']){
							$wallet = Wallet::where('bash_id', '=', $bash['id'])->where('type', '=', 'credit')->sum('amount');
							if(!empty($wallet))
							$earning[6]['earings'] += number_format($wallet,2);
						}
						
						if(date('Y-m-d',strtotime($currDate .'-6 days')) == $bsh_data['end_date']){
							$attendance[0]['male'] += $bsh_data['male'];
							$attendance[0]['fe_male'] += $bsh_data['fe_male'];
							$attendance[0]['not_specified'] += $bsh_data['not_specified'];
						}
						if(date('Y-m-d',strtotime($currDate .'-5 days')) == $bsh_data['end_date']){
							$attendance[1]['male'] += $bsh_data['male'];
							$attendance[1]['fe_male'] += $bsh_data['fe_male'];
							$attendance[1]['not_specified'] += $bsh_data['not_specified'];
						}
						if(date('Y-m-d',strtotime($currDate .'-4 days')) == $bsh_data['end_date']){
							$attendance[2]['male'] += $bsh_data['male'];
							$attendance[2]['fe_male'] += $bsh_data['fe_male'];
							$attendance[2]['not_specified'] += $bsh_data['not_specified'];
						}
						if(date('Y-m-d',strtotime($currDate .'-3 days')) == $bsh_data['end_date']){
							$attendance[3]['male'] += $bsh_data['male'];
							$attendance[3]['fe_male'] += $bsh_data['fe_male'];
							$attendance[3]['not_specified'] += $bsh_data['not_specified'];
						}
						if(date('Y-m-d',strtotime($currDate .'-2 days')) == $bsh_data['end_date']){
							$attendance[4]['male'] += $bsh_data['male'];
							$attendance[4]['fe_male'] += $bsh_data['fe_male'];
							$attendance[4]['not_specified'] += $bsh_data['not_specified'];
						}
						if(date('Y-m-d',strtotime($currDate .'-1 days')) == $bsh_data['end_date']){
							$attendance[5]['male'] += $bsh_data['male'];
							$attendance[5]['fe_male'] += $bsh_data['fe_male'];
							$attendance[5]['not_specified'] += $bsh_data['not_specified'];
						}
						if(date('Y-m-d',strtotime($currDate .'-0 days')) == $bsh_data['end_date']){
							$attendance[6]['male'] += $bsh_data['male'];
							$attendance[6]['fe_male'] += $bsh_data['fe_male'];
							$attendance[6]['not_specified'] += $bsh_data['not_specified'];
						}
					}
					$earning[0]['earings'] = number_format($earning[0]['earings'],2);
					$earning[1]['earings'] = number_format($earning[1]['earings'],2);
					$earning[2]['earings'] = number_format($earning[2]['earings'],2);
					$earning[3]['earings'] = number_format($earning[3]['earings'],2);
					$earning[4]['earings'] = number_format($earning[4]['earings'],2);
					$earning[5]['earings'] = number_format($earning[5]['earings'],2);
					$earning[6]['earings'] = number_format($earning[6]['earings'],2);
					
				}
			}
			
			
			if(!empty($bashArr)){
				$finalData = array('bash_data'=> $bashArr, 'earning'=> $earning, 'attendance'=> $attendance,'rate_data'=> $this->get_bash_rate_user($user_id,$bash_ids));
				api_response(200,'Data listed successfully',$finalData);
			}else{
				api_response(200,'No data found','');
			}
			
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
    public function weekly_bash_details(Request $request){
        
        if(!empty($_REQUEST['bash_id'])){
			
			$bash_id= $_REQUEST['bash_id'];
			$bsh_data = $this->get_bash_long($bash_id,'');
			$total_earned_amount = Wallet::where('bash_id', '=', $bash_id)->where('type', '=', 'credit')->sum('amount');
			$age_details = $this->get_bash_age($bash_id);
			$slots = $this->getServiceScheduleSlots('60', $bsh_data['start_date_time'],$bsh_data['end_date_time']);
			//print_r($slots); die;
			
			
			
			if(!empty($bsh_data)){
				$finalData = array('total_earned_amount'=>  number_format($total_earned_amount,2), 'age_details'=> $age_details['agegroup'], 'popular_time'=> !empty($slots)?$slots:array(),'average_time'=> !empty($age_details['avg_time'])?number_format($age_details['avg_time']/60,'2'):'0.00');
				api_response(200,'Data listed successfully',$finalData);
			}else{
				api_response(200,'No data found','');
			}
		}else{
            api_response(404,'Please fill all required data');
        }
    }
	
	private function getServiceScheduleSlots($duration, $start,$end)
    {       
            $start = substr_replace($start ,":00:00", -6);
			
			//$start = substr($start, 0, -6);
            //echo $start;
            $start_time = date('Y-m-d H:i:s', strtotime($start));
			$end_time = date('Y-m-d H:i:s', strtotime($end));
            
            $i=0;
            while(strtotime($start_time) <= strtotime($end_time)){
                $start = $start_time;
                $end = date('Y-m-d H:i:s',strtotime('+'.$duration.' minutes',strtotime($start_time)));
                $end = substr_replace($end ,":00:00", -6);
                
                if(strtotime($start_time) <= strtotime($end_time)){
					
					
					$bash_id = $_REQUEST['bash_id'];
					$currenthosts = LocationUpdate::where(function ($query) use ($start,$end_time,$bash_id) {
						$query->where('start_time', '>=', $start)
							  ->where('start_time', '<=', $end_time)
							  ->where('bash_id', '=', $bash_id);
					})->orWhere(function ($query) use ($start,$end_time,$bash_id) {
						$query->where('end_time', '>=', $start)
							  ->where('end_time', '<=', $end_time)
							  ->where('bash_id', '=', $bash_id);
					})->count();
					
					$time[] = array('start'=> $start, 'end'=> $end,'persons'=> $currenthosts);
                    $start_time = date('Y-m-d H:i:s',strtotime('+'.$duration.' minutes',strtotime($start_time)));
										
					$i++;					
                }
            }
            return $time;
    }
	
    public function notify_ride(Request $request){
        
        if(!empty($_REQUEST['user_id']) && !empty($_REQUEST['message']) ){
            try {
                $rides =  SafeRide::where('user_id', '=', $request->user_id)->first();
                $exsists =  Notify::where('user_id', '=', $request->user_id)->where('request_id', '=', $request->request_id)->first();
                if(!empty($exsists)){

                }else{
                    if(!empty($rides->users)) {
                        foreach(explode(',',$rides->users) as $unique_user){
                            $this->save_notify($unique_user,'Safe Ride',$_REQUEST['message'],'notify_ride');
                            $user = $this->get_user($unique_user);
                            if(!empty($user['phone_number']) && !empty($user['country_code']) ){
                                $phone = $user['country_code'].$user['phone_number'];
                                $this->sendSMS($phone, $_REQUEST['message']);
                            }
                        }
                    }
                }
                

                $notify = new Notify();
                $notify->user_id = $_REQUEST['user_id'];
                $notify->request_id = $_REQUEST['request_id'];
                $notify->save();
                api_response(200,'Notified successfully');

            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
        }else{
            api_response(404,'Please fill all required data');
        }
    }
    
	public function get_help(Request $request){
		if(!empty($_REQUEST['subject']) && !empty($_REQUEST['issue']) && !empty($_REQUEST['user_id'])){
			try {
                $user =  User::where('id', '=', $request->user_id)->first();
				$attachment = '';
				if(!empty($_FILES['attachment'])){

					if(isset($_FILES['attachment']) && $_FILES['attachment']['name'] !== '' && !empty($_FILES['attachment']['name'])){
						$file = $_FILES['attachment'];
						$file = preg_replace("/[^a-zA-Z0-9.]/", "", $file['name']);
						$filename = time().'-'.$file;
						//$ext = substr(strtolower(strrchr($file, '.')), 1); //get the extension
						//$arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                        $path="public/images/attachment/";
						if(move_uploaded_file($_FILES['attachment']['tmp_name'],$path.$filename)){
							 $attachment =  $path.$filename;
						}
					}
                }
			    
				$this->send_email('operations@bashmobileapp.com',$_REQUEST['subject'],array('name'=> $user->fname.' '.$user->lname,'phone'=> $user->phone_number,'username'=> $user->username,'email'=> $user->email,'issue'=> $_REQUEST['issue']),$attachment);
				api_response(200,'Report submitted successfully.');
            }catch (\Illuminate\Database\QueryException $exception){
                $errorCode = $exception->errorInfo[1];
                api_response(404,$exception->getMessage());
            }catch(\Exception $exception){
                api_response(404,$exception->getMessage());
            }
		}else{
			api_response(404,'Please fill all required data');
		}
	}
	
	public function deduct_face(Request $request){
		require_once "public/HTTP_Request/vendor/autoload.php";
		require "public/HTTP_Request/HTTP/Request2.php";
        
		
        if( !empty($_REQUEST['user_id'])){
			$ocpApimSubscriptionKey = '111f2e2bf7114adb8dfe2cbb4d9a7305';
            $uriBase = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/';
            $imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/3/37/Dagestani_man_and_woman.jpg';
	        $request = new \Http_Request2($uriBase . '/detect');
			$url = $request->getUrl();
           
			$headers = array(
				// Request headers
				'Content-Type' => 'application/json',
				'Ocp-Apim-Subscription-Key' => $ocpApimSubscriptionKey
			);
			$request->setHeader($headers);

			$parameters = array(
				// Request parameters
				'returnFaceId' => 'true',
				'returnFaceLandmarks' => 'false',
				'returnFaceAttributes' => 'age,gender,headPose,smile,facialHair,glasses,' .
					'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise');
			$url->setQueryVariables($parameters);

			$request->setMethod(HTTP_Request2::METHOD_POST);

			// Request body parameters
			$body = json_encode(array('url' => $imageUrl));

			// Request body
			$request->setBody($body);

			try
			{
				$response = $request->send();
				//echo "<pre>" .
					//json_encode(json_decode($response->getBody()), JSON_PRETTY_PRINT) . "</pre>";
			}
			catch (\HttpException $ex)
			{
				//echo "<pre>" . $ex . "</pre>";
			}
            die;
            if(!empty($datas)){
                    $finalData = array();
                      foreach($datas as $data){ 
                        $finalData[] = array('id'=> $data['id'],'fname'=> $data['fname'],'lname'=> $data['lname'],'username'=> $data['username']);
                      }
                      api_response(200,'Data listed successfully',$finalData);
                }else{
                    api_response(200,'No data found','','array');
                }
				
        }else{
            api_response(404,'Please fill all required data');
        }
    }
	
    private function add_wallet($user_id,$amount,$type,$bash_id=null,$payout_item_id=null,$transaction_status=null,$transcation_id=null){
        
        $wallet = new Wallet();
        $q = Wallet::query();
        $q->where('user_id','=',$user_id);
        $wallet_data = $q->orderBy('id','DESC')->first();
        //print_r($wallet_data); die;
        if($type == 'credit'){
            if(!empty($wallet_data->amount)){
                $wallet->total_amount = $wallet_data->total_amount+$amount;
            }else{
                $wallet->total_amount = $amount;
            }
        }else{
            if(!empty($wallet_data->amount)){
                $wallet->total_amount = $wallet_data->total_amount-$amount;
            }else{
                $wallet->total_amount = $amount;
            }
        }
        
        
        $wallet->user_id = $user_id;
        //$wallet->bash_id = $bash_id;
        $wallet->amount = $amount;
        $wallet->type = $type;

        if(!empty($bash_id)){
            $wallet->bash_id = $bash_id;
        }
        if(!empty($payout_item_id)){
            $wallet->payout_item_id = $payout_item_id;
        }
        if(!empty($transcation_id)){
            $wallet->transcation_id = $transcation_id;
        }
        if(!empty($transaction_status)){
            $wallet->transaction_status = $transaction_status;
        }
        //print_r($wallet); die;
        //$wallet->temp = $temp;
        $wallet->save();
    }
    
    private function add_temp_wallet($user_id,$from_id,$amount,$type,$bash_id){
        $wallet = new TempWallet();
        $q = TempWallet::query();
        $q->where('user_id','=',$user_id);
        $wallet_data = $q->orderBy('id','DESC')->first();
		$amount_tax = ($amount*6.5)/100;
		$wallet->total_paid = $amount;
		$wallet->tax = $amount_tax;
		$amount = $amount-$amount_tax;
        if(!empty($wallet_data->amount)){
            $wallet->total_amount = $wallet_data->total_amount+$amount;
        }else{
            $wallet->total_amount = $amount;
        }
        
        $wallet->user_id = $user_id;
        $wallet->from_id = $from_id;
        $wallet->bash_id = $bash_id;
        $wallet->amount = $amount;
        $wallet->type = $type;
        //$wallet->temp = $temp;
        $wallet->save();
    }

    private function add_qr_code($ticket_id,$bash_id,$user_id){
        $qr = new Qrcode();
        $qr->ticket_id = $ticket_id;
        $qr->bash_id = $bash_id;
        $qr->user_id = $user_id;
        $qr->code = strtoupper(Keygen::alphanum(8)->generate());
        $qr->save();
    }

    private function send_verify_email_otp($email_to, $message){
        //$email_to = 'himanshukumar.orem@gmail.com';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->setFrom("zak@bashmobileapp.com", 'BASH');
        $mail->addAddress($email_to, 'Verify OTP');
        $mail->Username = "AKIA42MJ2LW77F5RL77K";
        $mail->Password = "BE3CZp49YeHR9MjSfi/gV2Mb+gK3bTfTTegrz1wURY6B";
        $mail->Host = "email-smtp.us-east-1.amazonaws.com";
        $mail->Subject = 'Your BASH verification code';
        $view = View::make('emails.email_otp', ['email'=> $email_to, 'otp' => $message]);
        $view_string = $view->render();
		
        $mail->Body = $view_string;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->isHTML(true);
        
		$message = 'Hi your otp code is '.$message; 
		
        $mail->AltBody = $message;
        
        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }

    private function send_verify_email($email_to, $token){

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->setFrom("zak@bashmobileapp.com", 'BASH');
        $mail->addAddress($email_to, 'New User');
        $mail->Username = "AKIA42MJ2LW77F5RL77K";
        $mail->Password = "BE3CZp49YeHR9MjSfi/gV2Mb+gK3bTfTTegrz1wURY6B";
        $mail->Host = "email-smtp.us-east-1.amazonaws.com";
        $mail->Subject = 'Verify Your BASH Registration';

        $view = View::make('emails.verify_email', ['email'=> $email_to, 'token' => $token]);
        $view_string = $view->render();

        $plain_text_view = View::make('emails.verify_email_plaintext', ['email'=> $email_to, 'token' => $token]);
        $plain_text_string = $plain_text_view->render();

        $mail->Body = $view_string;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->isHTML(true);

        $mail->AltBody = $plain_text_string;

        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
    
	private function send_email($email_to, $subject, $data,$attachment=null){

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->setFrom("zak@bashmobileapp.com", 'BASH');
        $mail->addAddress($email_to, 'New User');
        $mail->Username = "AKIA42MJ2LW77F5RL77K";
        $mail->Password = "BE3CZp49YeHR9MjSfi/gV2Mb+gK3bTfTTegrz1wURY6B";
        $mail->Host = "email-smtp.us-east-1.amazonaws.com";
        $mail->Subject = $subject;
        if(!empty($attachment)){
			$mail->addAttachment($attachment);
		}
        $view = View::make('emails.submit_help', ['email'=> $email_to, 'data' => $data]);
        $view_string = $view->render();

        //$plain_text_view = View::make('emails.verify_email_plaintext', ['email'=> $email_to, 'token' => $token]);
        //$plain_text_string = $plain_text_view->render();
        
        $mail->Body = $view_string;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        $mail->isHTML(true);

        //$mail->AltBody = $plain_text_string;

        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
	

    protected function sendSMS($phone_number, $message = ''){
        //echo $phone_number; die;
        $message = urldecode($message);
       
        $twilio = new Client('AC7a4e71d6a33f4f6e1ecf29f466b37f6f', 'f1475b628cdc43185a130b8c95f019ad');
        //echo env('JWT_SECRET'); die;
        try{
            $message_obj = $twilio->messages
                ->create($phone_number, // to
                    array("from" => '+12024100600', "body" => $message)
                );
            //print_r($message_obj); die;    
        }catch(\Twilio\Exceptions\RestException $e){
            if($e->getCode() == 21211){
                //echo "invalid number for sms"; die;
                //return array(false, 'invalid number for sms');
            }
        }catch(\Exception $e){
            //echo "invalid number for sms1"; die;
            //return array(false, 'other texting ergitror');
        }
//echo $phone_number; die;
        //echo json_encode(array('status' => true));
        return array(true, 'success');
    }
    
     
    function nullvalue_filter($data){		
		$filtereddata = array();
		if(!empty($data)){
			foreach($data as $key1=>$value1){
				if(is_object($value1) || is_array($value1)){
					foreach($value1 as $key2=>$value2){
						if(is_array($value2) || is_object($value2)){
							foreach($value2 as $key3=>$value3){
								if(is_array($value3) || is_object($value3)){									
									$filtereddata[$key1][$key2][$key3] = $value3;
								}else{
									if(is_null($value3)){
										$filtereddata[$key1][$key2][$key3] = "";
									}else{
										$filtereddata[$key1][$key2][$key3] = $value3;
									}
								}
							}
						}else{
							if(is_null($value2)){
								$filtereddata[$key1][$key2] = "";
							}else{
								$filtereddata[$key1][$key2] = $value2;
							}
						}
					}
				}else{
					if(is_null($value1)){
						$filtereddata[$key1] = "";
					}else{
						$filtereddata[$key1] = $value1;
					}					
				}
			}
		}else{
			$filtereddata = array();
		}
		return $filtereddata;
    }
	public function sortarray($finalData){
		//print_r($finalData); die;
        $distance  = array_column($finalData, 'distance');
        $ratings = array_column($finalData, 'ratings');
        $complement = array_column($finalData, 'complement');
        $nofpeople = array_column($finalData, 'male')+array_column($finalData, 'fe_male')+array_column($finalData, 'not_specified');

        if(isset($_REQUEST['order']) && $_REQUEST['order']=='asc'){
            return array_multisort($ratings, SORT_ASC, $complement, SORT_ASC, $nofpeople, SORT_ASC, $distance, SORT_ASC, $finalData); 
        }else{
            return array_multisort($ratings, SORT_DESC, $complement, SORT_DESC, $nofpeople, SORT_DESC, $distance, SORT_DESC, $finalData); 
        }
    }
	
	public function dashboard(){
		$registered =  User::count();;
		$active =  User::whereDate('last_location', '=', date('Y-m-d'))->count();;
		$data = array('active'=>$active,'registered'=>$registered);
		return View::make('dashboard')->with('data', $data);

	}
	
	public function bash_detailed_data(Request $request){
		 $result = "request of crash list "; 
		//$result .= json_encode($_REQUEST);
		//\Log::info(print_r($result, true));
		
        if(!empty($_REQUEST['user_id']) && !empty($_REQUEST['bash_id']) ){
            try{
                $user_id = $_REQUEST['user_id'];
				$bash_id = $_REQUEST['bash_id'];
				
				$checkin_datas =  Crash::where('bash_id', '=', $_REQUEST['bash_id'])->get()->toArray();
				$current_bash_user_arr = array();
				
				if(!empty($checkin_datas)){
					foreach($checkin_datas as $res){
						$userData = $this->nullvalue_filter($this->get_user($res['user_id']));
						$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['user_id'] )->count();
						if(!empty($follow_status)){
							$userData['follow'] = 1;
						}else{
							$userData['follow'] = 0;
						}
						$userData['no_of_ticket_purchase'] = '0';
						$current_bash_user_arr[] = 	$userData;
					}
				}
				
				$ticket_buy_datas =  Ticket::where('bash_id', '=', $_REQUEST['bash_id'])->get()->toArray();
				if(!empty($ticket_buy_datas)){
					foreach($ticket_buy_datas as $res){
						$userData = $this->nullvalue_filter($this->get_user($res['user_id']));
						$follow_status = Follow::where('user_id','=',$_REQUEST['user_id'] )->where('other_id','=',$res['user_id'] )->count();
						if(!empty($follow_status)){
							$userData['follow'] = 1;
						}else{
							$userData['follow'] = 0;
						}
						$userData['no_of_ticket_purchase'] = $res['qty'];
						$current_bash_user_arr[] = 	$userData;
					}
				}
				
                if(!empty($current_bash_user_arr)){
					api_response(200,'',$current_bash_user_arr);
				}else{
					api_response(200,'No data found','','array');
				}
                
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        }else{
            api_response(404,'Please fill all required data');
        }
    }
	
	public function hosted_data(Request $request){
		// hosted,attendant,followes,following
		
        if(!empty($_REQUEST['user_id']) && !empty($_REQUEST['bash_id']) ){
            try{
                $user_id = $_REQUEST['user_id'];
				$bash_id = $_REQUEST['bash_id'];
				$current_bash_user_arr = array();
				
				if($_REQUEST['type'] == 'followes' ){
					$checkin_datas = Follow::where('other_id','=',$user_id )->get()->toArray();
					if(!empty($checkin_datas)){
						foreach($checkin_datas as $res){
							$userData = $this->nullvalue_filter($this->get_user($res['user_id']));
							$current_bash_user_arr[] = 	$userData;
						}
					}
				}
				
                if(!empty($current_bash_user_arr)){
					api_response(200,'',$current_bash_user_arr);
				}else{
					api_response(200,'No data found','','array');
				}
                
            }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $exception){
                api_response(404,'Error in server');
            }

        }else{
            api_response(404,'Please fill all required data');
        }
    }
									   
}