<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AWS;

use Twilio\Rest\Client;

class SMSController extends Controller
{

    public function sendSMSTwilio(){

        $phone_number = $_REQUEST['phone_number'];
        $phone_number = "+1".$phone_number;
        $message = $_REQUEST['message'];
        $message = urldecode($message);
        $twilio = new Client(env('TWILIO_SID'), env('TWILIO_TOKEN'));

        $message_obj = $twilio->messages
            ->create($phone_number, // to
                array("from" => env('TWILIO_FROM'), "body" => $message)
            );

        echo json_encode(array('status' => true));
        exit;
    }
}


