<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationUpdate extends Model
{
    protected $table = "location_updates";
   
    public function bash()
    {
    	return $this->belongsTo('App\Bash', 'bash_id');
    }

}