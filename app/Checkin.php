<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    protected $table = "checkins";

    public function bash()
    {
    	return $this->belongsTo('App\Bash', 'bash_id');
    }

}
