<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bash extends Model
{
    protected $table = "bashes";

    public function crashes()
    {
        return $this->hasMany('App\Crash', 'bash_id');
    }

}
