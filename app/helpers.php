<?php

function api_response($status,$status_message,$data=null,$type=null) {
    $status_message = (string) $status_message;
    $status_string = $status . ' ' . $status_message;
    header($_SERVER['SERVER_PROTOCOL'] . ' ' . rawurlencode($status_string), true, $status);
    $results = array();
    if(!empty($status_message)){

    }
    if(!empty($data)){
        $results['data'] = $data;
        $results['mesg'] = $status_message;
    }else{
        if($type == 'array'){
            $results['data'] =  array();
        }else{
            $results['data'] =  new \stdClass();
        }
        
        $results['mesg'] = $status_message;
    }

    echo json_encode($results);
    exit;
}

//print_r($_REQUEST); die;
// if(!empty($_REQUEST['timezone'])){
//     $timezone = $_REQUEST['timezone'];
    
// }else{
//     $timezone = 'Europe/London';
    
// }


 function time_elapsed_string($datetime, $full = false) {
    //error_reporting(-1);
    //ini_set('display_errors', 1);

    $language = !empty($_REQUEST['lang'])?$_REQUEST['lang']:'en';
	if(!empty($_REQUEST['timezone'])){
		$userTimezone = new DateTimeZone($_REQUEST['timezone']);
	}else{
		$userTimezone = new DateTimeZone('GMT');
	}
	
    $gmtTimezone = new DateTimeZone('GMT');

    $now = new DateTime('',$gmtTimezone);
	$offset_now = $userTimezone->getOffset($now);
	$myInterval=DateInterval::createFromDateString((string)$offset_now . 'seconds');
    $now->add($myInterval);

    $ago = new DateTime($datetime,$gmtTimezone);
	$offset_ago = $userTimezone->getOffset($ago);
	$myInterval_ago=DateInterval::createFromDateString((string)$offset_ago . 'seconds');
    $ago->add($myInterval_ago);
	
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
  
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
    
} 

 function send_notification($reg_id,$title,$msg,$type=null,$bash_id=null) {
        $key = 'AIzaSyDqH4WE_y7Ma7RNyGy-0zaOY7DEQTBlmxE';
        $message = array
        (
            'message' => ($msg) ? $msg : '',
            'title' => $title,
            'type' => ($type) ? $type : '',
            'bash_id' => ($bash_id) ? $bash_id : '',
        );
        
        $fields = array
        (
            'to' => $reg_id,
            'data' => $message,
            'priority' => 'high'
        );

        $headers = array
            (
            "Authorization: key=$key",
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //pr($result); die;
        curl_close($ch);
        if ($result === FALSE) {
           // die('Curl failed: ' . curl_error($ch));
        } else {
           // $data = array('message' => 'success');
           //json_encode($data);
        }
       return true;
  }
  
   function send_inotification($reg_id,$title,$msg,$type=null,$bash_id=null) {
   
        $passphrase = 'Orem@123';

        $ctx = stream_context_create();
        
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'public/Bash.pem');
		//stream_context_set_option($ctx, 'ssl', 'local_cert', 'public/BashDevPsuh.pem');
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
    
        //stream_context_set_option($ctx, 'ssl', 'local_cert', $iosData['live_pem_file']);
        //$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        
        if (!$fp)
        exit("Failed to connect: $err $errstr" . PHP_EOL);
    
        $body['aps'] = array(
            'alert' => array("title"=>$title,"body"=>$msg),
            'sound' => 'default',  
            'content-available'=>true,
        );
        
        $body['type'] = ($type) ? $type : '';
        if(!empty($bash_id)){
            $body['bash_id'] = ($bash_id) ? $bash_id : '';
        }
        
        // Encode the payload as JSON
        //print($msg); die;
        $payload = json_encode($body);
        // Build the binary notification

        $msg = chr(0) . pack('n', 32) . pack('H*', $reg_id) . pack('n', strlen($payload)) . $payload;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        //pr($result); die;
        fclose($fp);
   
    
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);
  
    if ($unit == "K") {
        return number_format((float)($miles * 1.609344), 2, '.', '');
    } else if ($unit == "N") {
        return number_format((float)($miles * 0.8684), 2, '.', ''); 
    } else {
		return number_format((float)$miles, 2, '.', ''); 
    }
}
