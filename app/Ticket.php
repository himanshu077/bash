<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = "tickets";

    public function qrcode()
    {
    	return $this->hasMany('App\Qrcode', 'ticket_id');
    }
    public function bash()
    {
    	return $this->belongsTo('App\Bash', 'bash_id');
    }

}
