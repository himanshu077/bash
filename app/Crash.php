<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crash extends Model
{
    protected $table = "crashes";
   
    public function bash()
    {
    	return $this->belongsTo('App\Bash', 'bash_id');
    }

}