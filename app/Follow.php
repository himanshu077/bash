<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    
  public function fromuser()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
    
  public function touser()
    {
    	return $this->belongsTo('App\User', 'other_id');
    }
	
}
