<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempWallet extends Model
{
    protected $table = "temp_wallets";

    public function bash()
    {
    	return $this->belongsTo('App\Bash', 'bash_id');
    }

}
