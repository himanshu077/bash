<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    
    //Config::set('app.timezone', 'Asia/Calcutta');
   
    return $request->user();
});




/* ping/pong debug */
Route::post('ping', 'ApiController@ping');
Route::get('ping', 'ApiController@ping');
Route::get('get_location_by_session_id', 'ApiController@get_user_location_by_session_id');
Route::get('radius', 'PingPong@radius');
Route::get('raw_radius', 'PingPong@raw_radius');
Route::get('get_all_redis', 'PingPong@debug_get_all');


Route::get('debug/init_map', 'PingPong@init_map');
Route::get('debug/get_bash', 'PingPong@get_bash');
Route::get('debug/get_user', 'PingPong@get_user');
Route::get('debug/get_crash', 'PingPong@get_crash');

Route::get('get_location', 'PingPong@get_location');

/* login and register, no token required */
Route::post('/login', 'ApiController@login');
Route::get('/login', 'ApiController@login'); 
Route::post('/register', 'ApiController@register');
Route::get('/register', 'ApiController@register');
Route::post('/send_otp', 'ApiController@send_otp');
Route::get('/send_otp', 'ApiController@send_otp');



Route::get('/check_username', 'ApiController@check_username')->name('check.username');
Route::get('/suggest_username', 'ApiController@suggest_username')->name('check.name');
/* alternate way of doing forgot pw... */
//Route::post('forgot/password', 'ForgotPasswordController')->name('forgot.password');
Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::get('password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('email/verify', 'ApiController@verify_email_do')->name('email.verify');

Route::get('/api_social_login', 'ApiController@api_social_login');
Route::post('/api_social_login', 'ApiController@api_social_login');
Route::get('/api_social_register', 'ApiController@api_social_register');
Route::post('/api_social_register', 'ApiController@api_social_register');
Route::post('/reset_password', 'ApiController@reset_password');

Route::post('forgot', 'ForgotPasswordController@forgot');
Route::get('forgot', 'ForgotPasswordController@forgot');
Route::post('reset', 'ResetPasswordController@reset');
Route::get('reset', 'ResetPasswordController@reset');
Route::post('get_token', 'ApiController@get_token');

// _________________//
   Route::post('/face_color_list', 'ApiController@face_color_list');
   Route::post('/face_match', 'ApiController@face_match');
   Route::get('/weekly_bash_details', 'ApiController@weekly_bash_details');
   Route::get('/followed_user_list', 'ApiController@followed_user_list');
   Route::get('/users_contact_list_home', 'ApiController@users_contact_list_home');
   Route::post('/withdraw_money', 'ApiController@withdraw_money');
   Route::get('/get_bash_details', 'ApiController@get_bash_details');
   Route::get('/user_follow_both', 'ApiController@user_follow_both');
   Route::post('/create_bash', 'ApiController@create_bash');
   Route::get('get_user_data', 'ApiController@get_user_data');
   Route::post('/calender_list', 'ApiController@calender_list');
   Route::post('/barcode_check_in', 'ApiController@barcode_check_in');
   Route::post('/follow', 'ApiController@follow');
   Route::post('/bash_list', 'ApiController@bash_list');
   Route::post('/get_crash_list', 'ApiController@get_crash_list');
   
   
// _________________//

Route::group(['middleware' => 'jwt.verify'], function () {
    Route::get('logout', 'ApiController@logout');
    Route::post('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');
    Route::post('update_profile', 'ApiController@update_profile');
    Route::post('crash_the_bash', 'ApiController@crash_the_bash');
    Route::post('/update_token', 'ApiController@update_token');
    Route::post('/update_paypal', 'ApiController@update_paypal');
    Route::get('init_map', 'PingPong@init_map');
    Route::get('get_bash', 'PingPong@get_bash');
    Route::get('get_user', 'PingPong@get_user');
    Route::get('get_crash', 'PingPong@get_crash');
	
	Route::post('/update_prefrence', 'ApiController@update_prefrence');
	
	
	
	Route::get('followers_list', 'ApiController@followers_list');
	Route::get('near_by', 'ApiController@near_by');
	Route::post('/add_to_bash', 'ApiController@add_to_bash');
	Route::post('/buy_ticket', 'ApiController@buy_ticket');
	
	Route::get('/get_check_in_list', 'ApiController@get_check_in_list');
	
	
	Route::get('bash_users_list', 'ApiController@bash_users_list');
	Route::get('users_list', 'ApiController@users_list');
	Route::get('delete_bash', 'ApiController@delete_bash');
	Route::post('update_bash', 'ApiController@update_bash');
	Route::post('free_check_in', 'ApiController@free_check_in');
	Route::post('give_rate', 'ApiController@give_rate');
	
	Route::get('/search', 'ApiController@search');
	Route::post('/add_ride', 'ApiController@add_ride');
	Route::post('/notify_ride', 'ApiController@notify_ride');
	Route::post('/generateToken', 'ApiController@generateToken');
	
	
	Route::get('/users_list_home', 'ApiController@users_list_home');
	
	Route::get('/search_users', 'ApiController@search_users');
	Route::get('/search_bash', 'ApiController@search_bash');
	Route::get('/users_contact_list', 'ApiController@users_contact_list');
	Route::post('/users_contact_list', 'ApiController@users_contact_list');
	
	Route::post('/submit_report', 'ApiController@get_help');
	Route::get('/weekly_payment_details', 'ApiController@weekly_payment_details');
	
	Route::post('/update_location', 'ApiController@update_location');

});