<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('apps/{id}', 'ApiController@redirect');
Route::get('eventapps/{id}', 'ApiController@redirect1');
Route::get('/test/sendSMSTest', 'SMSController@sendSMSTwilio');
Route::get('/sendTestEmail/{email}', 'EmailController@sendTestEmail');

Route::get('/testBashes', 'TestBashesOneToOne@testBashes');
Route::get('/cron1', 'ApiController@cron1');
Route::get('/cron2', 'ApiController@cron2');
Route::get('/cron4', 'ApiController@cron3');
Route::get('/cron5', 'ApiController@cron5');
Route::get('password/show_reset', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('/dashboard', 'ApiController@dashboard');

Route::get('/privacy', function () {
    return view('privacy');
});


Route::get('/terms', function () {
    return view('terms');
});

Route::get('/help', function () {
    return view('help');
});
